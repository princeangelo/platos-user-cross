import { Component } from '@angular/core';

/**
 * Generated class for the MenuDetailComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'menu-detail',
  templateUrl: 'menu-detail.html'
})
export class MenuDetailComponent {

  text: string;

  constructor() {
    console.log('Hello MenuDetailComponent Component');
    this.text = 'Hello World';
  }

}
