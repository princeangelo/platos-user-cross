import { Component, ViewChild } from '@angular/core';
import { App,Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProvider } from '../providers/user/user';
import {
    FirstRunPage
} from '../pages/pages';
import {
    MainPage
} from '../pages/pages';
import {
    File
} from '@ionic-native/file';
import { AlertProvider } from '../providers/alert/alert';
import { NgZone } from '@angular/core';
import { Events } from 'ionic-angular';
import { Api } from '../providers/api/api';

@Component({
  templateUrl: 'app-nav.html',
   styles: ["app-nav.scss"]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  image:'../assets/imgs/cater_banner.png'
  pages: Array<{title: string, component: any}>;
  user_name:any
  user_mail:any
  
  filelocation:any
is_Admin:any
imgPath:any

profilePic:any
defaultPic = "assets/imgs/cater_banner.png"

  constructor(public api:Api,public events: Events,public _zone: NgZone,public alert:AlertProvider,public file:File,public app:App,public user: UserProvider,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    



      this.imgPath = this.api.profile_pic;
      
      let pic = localStorage.getItem("pic");
      if(pic){
        this.defaultPic = this.imgPath + pic;
      }
      

// events.subscribe('profilepic:created', (profile) => {
//     this._zone.run(() => { 
//     console.log('picName', profile);
//     localStorage.setItem("pic",profile);
//     this.defaultPic = this.imgPath + profile;
//   })
    
//   });

events.subscribe('isAdmin:created', (admin) => {
    this._zone.run(() => { 
    console.log('admin', admin);
    localStorage.setItem("is_admin", admin);
    let temp = localStorage.getItem("is_admin")
    this.is_Admin = temp;
  })

  });

events.subscribe('login:created', (name, mail) => {
     this._zone.run(() => { 
    console.log('Welcome', name, mail);
    this.user_name = name;
    this.user_mail = mail;
    localStorage.setItem("user_name",  this.user_name)
    localStorage.setItem("user_email",  this.user_mail)
     this.is_Admin = localStorage.getItem("is_admin")
  })

  });

    this.initializeApp();

    // used for an example of ngFor and navigation
  // this.pages = [

  //    { title: 'Home', component: DashboardPage },
  //    { title: 'Menus', component: MenusPage },
  //    { title: 'Orders', component: OrdersPage },
  //    { title: 'My-Profile', component: MyProfilePage },
  //    { title: 'Vendor', component: VendorPage },
  //  ];
  }

  initializeApp() {

    this.platform.ready().then(() => {

let token = localStorage.getItem("token")
       if(token){
      debugger
      this._zone.run(() => { 
      let name = localStorage.getItem("user_name");
      let mail = localStorage.getItem("user_email");
      this.events.publish('login:created', name, mail);
      console.log("when logged in",name,mail)
    })
    }


     let pic = localStorage.getItem("pic");
      if(pic){
        let profile = pic
        this.events.publish('profilepic:created', profile);
      }

  this.is_Admin = localStorage.getItem("is_admin")
       if(this.platform.is('ios')){
        console.log("ios true")
         this.filelocation = this.file.cacheDirectory;
        this.user.setDownloadPath(this.filelocation)
      }

      if(this.platform.is('android')){
        console.log("android true")
        this.filelocation = this.file.externalDataDirectory;
        this.user.setDownloadPath(this.filelocation)
      }
         
            // this._zone.run(() => { 




      //     this.user.getUsername(id).subscribe((data: any) => {
      // console.log(data,"dash")
     
      // localStorage.setItem("carter_name",  data.data.catt_first_name)
      // localStorage.setItem("carter_email",  data.data.catt_email_address)

      //   }, (error) => {
      //      console.log(error)
      //   })

            // })
        this.startApp()
        this.statusBar.backgroundColorByHexString('#c2953b');
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page);
  }

    quit() {
        // this.shared.Alert.confirm(this.TrnsData["QUIT"]).then((res) => {
            this.platform.exitApp();
        // }, err => {})
    }

        startApp() {
        if (this.user.checkLogin()) {
            this.rootPage = MainPage;
            // this.menu.enable(false);
        } else {
           this.rootPage = FirstRunPage;        
         }
    }

   async logout(){

         // this.alert.logout().then((res)=>{
        await    this.user.logout();
            this.app.getRootNav().setRoot(FirstRunPage)
          // },err => {
          //   console.log(err)
          // })   
     }
        
}
