import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { NgZone } from '@angular/core';
import { Events } from 'ionic-angular';
/**
 * Generated class for the OrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {

defaultMenuPic = "assets/imgs/no_image.jpg"
flagStatus:any = false
menuarray:any = []
searchText=""
OrgItems
imgPath:any
mealTypes:any

Cart_Count:any = 0;
page_count = 0;
totalPages:any
infiniteScrollEnabled = true
Cater_Img:any

  constructor(public events: Events,public _zone: NgZone,public navCtrl: NavController, public navParams: NavParams,public api :Api,public user: UserProvider,public alert : AlertProvider) {
  this.imgPath = this.api.Menu_image;
  this.Cater_Img = this.api.filepath;

  events.subscribe('cart:refresh', (count) => {
    this._zone.run(() => { 
    this.Cart_Count = count;
    console.log("Got data from cart")
  })

  });

  }

  ionViewDidLoad() {
  	this.getOrderList();
    this.getCount()
    console.log('ionViewDidLoad OrderPage');
  }

 getOrderList(){

   

 this._zone.run(() =>  {
   this.alert.Loader.show("Loading")
   this.page_count = 1;
   let valu = {
    "token" : localStorage.getItem("token"),
    "page_no": this.page_count,
    "search": "",
}
    this.user.getOrder(valu).subscribe((data: any) => {
    // alert(JSON.stringify(data.data));
            if (data.status == 'ok') {
               this.menuarray = data.data;
                for(let i = 0;i< this.menuarray.length ; i++){
                  this.menuarray[i].wishlist = false
                }

         if(this.menuarray.length > 0){
         let temp:any =  this.menuarray[0].total / 10
         if((this.menuarray[0].total % 10) != 0){
             this.totalPages = parseInt(temp) + 1;      
         }
         else{
           this.totalPages = parseInt(temp)
         }
       }

       console.log(this.totalPages)


                this.OrgItems = data.data
                 // console.log(JSON.stringify(data.data),"Value see")
                   this.alert.Loader.hide();
        this.user.getMealTypes().subscribe((data: any) => {
            if (data.status == 'ok') {
              this.mealTypes = data.data;
              for(let j = 0;j<this.mealTypes.length;j++){
                // console.log(data.data)
                this.mealTypes[j].selected = false
              }
                   this.alert.Loader.hide();
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
   })
 }

 LoadData(event){
    this._zone.run(() =>  {
   this.page_count = this.page_count + 1;
   let valu = {
    "token" : localStorage.getItem("token"),
    "page_no": this.page_count,
    "search": "",
}
 if(this.page_count <= this.totalPages){
    this.user.getOrder(valu).subscribe((data: any) => {
            if (data.status == 'ok') {
              //console.log(JSON.stringify(data.data),"Value see123")
          for(let j=0;j<data.data.length;j++){
             this.menuarray.push(data.data[j])
           this.OrgItems.push(data.data[j]);
          }

                for(let i = 0;i< this.menuarray.length ; i++){
                  this.menuarray[i].wishlist = false
                }

                   this.alert.Loader.hide();
                     if(this.page_count != this.totalPages){
                      event.complete();
                            }
                            else{
                               this.infiniteScrollEnabled = false;
                            }
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
      else {
        console.log("in else")
         this.infiniteScrollEnabled = false;
       // this.alert.Alert.alert("Reached End Of the Page")
    }

   })
    
 }

 getImage(value){
 let data = this.Cater_Img+value;
 return data
 }

 menuDetail(value:any){
   console.log("detail clicked")
   this.navCtrl.push("CatererDetailPage",{
     data:value
   })
 }


       onFilter(far) {
         console.log(this.searchText)
      this.menuarray = this.OrgItems;
      
       this.menuarray =  this.menuarray.filter((menu) => {
            if (menu.catt_first_name.toLocaleLowerCase().includes(this.searchText.toLocaleLowerCase()) ) {
                return menu
            }
        })
    }
    onCancel(far) {
        this.menuarray = [];
        this.OrgItems = [];
        console.log("Load ...");
        this.getOrderList();
    }

    getCount(){
 this._zone.run(() =>   {
  let token = localStorage.getItem("token")
         this.user.Notification(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               // this.alert.Toast.show("Added to Cart")
               console.log(data,"notification data")
               this.Cart_Count = data.data.cart_count
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    })
 
}

buy(){
  this.navCtrl.push("MyCartPage")
}

filterPage(){
  this.navCtrl.push("FilterPage")
}


}
