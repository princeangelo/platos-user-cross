import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform, ActionSheetController, Content } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { PhotoViewer } from '@ionic-native/photo-viewer';

/**
 * Generated class for the UserinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-userinfo',
  templateUrl: 'userinfo.html',
})
export class UserinfoPage {
  @ViewChild(Content) content: Content;
  ShippingAddress:any =[]
  BillingAddress:any = []
  profileData:any = {
    name:'',
    phone:'',
    mail:''
  }
  Cart_Count:any = 0
  id:any
  ishidden:boolean=false;
  vendorData: any
    Address: any
    Food_certi: any
    GST_path: any
    PAN_path: any
    is_Admin: any
    Category: any = ""
    catge_flag = false
    showMinus = false
    groupname: any = ""
    showaddGroup: any = false;
    GroupNames: any
    Gropmenus: any
    ShowMinus: any = false
    certifi_delete: any = false
    pincodes: any
    DeliveryDays: any
    Certificate: any = []
    Holidays: any
    Cater_Img: any
    public alertPresented: any;
    Golive: any = false
    status: any
    show: any = false
    hasmorethanfive: any = false
    hasmorethanfifteen: any = false
    showAllPin: any = false
    showLess: any = false
    anArrays: any
    constructor(public api :Api,public user: UserProvider,public alert : AlertProvider,public events: Events,public _zone: NgZone,public navCtrl: NavController, public navParams: NavParams
     , public photoViewer: PhotoViewer, public platform: Platform, public actionSheetCtrl: ActionSheetController) {
      this.is_Admin = localStorage.getItem("is_admin")
      console.log(this.is_Admin)
      this.Cater_Img = this.api.filepath;
      this.alertPresented = false
  
      events.subscribe('cart:refresh', (count) => {
      this._zone.run(() => { 
      this.Cart_Count = count;
      console.log("Got data from cart")
    })
   })   
    }
  
    ionViewDidLoad() {
      this.getCount()
    }
  
    editProfile(){
      this.navCtrl.push("ProfileEditPage",{
        data : this.profileData,
        id : this.id,
        "page":"user_info"
      })
    }
  
          getCount(){
   this._zone.run(() =>   {
    let token = localStorage.getItem("token")
           this.user.Notification(token).subscribe((data: any) => {
              if (data.status == 'ok') {
                 // this.alert.Toast.show("Added to Cart")
                 console.log(data,"notification data")
                 this.Cart_Count = data.data.cart_count
  
                }
              else {
                  this.alert.Alert.alert(data.data)
                  return;
              }
          }, (error) => {
              this.alert.Loader.hide();
              this.alert.Alert.alert("Cannot Connect To Server")
              return;
          })
      })
   
  }
  
  AddDelivery(){
   this.navCtrl.push("AddShipaddressPage",{
     from:"delivery"
   })
  }
  
  AddBilling(){
   this.navCtrl.push("AddShipaddressPage",{
     from:"billing"
   })
  }
  
  // ChangePass(){
  //   this.navCtrl.push("ChangepasswordPage",{
  //     id: this.id
  //   })
  // }
  
    getDetails(){
      this.alert.Loader.show("Loading..")
       this._zone.run(() =>   {
          let token = localStorage.getItem("token")
                  this.user.UserDetails(token).subscribe((data: any) => {
                  if (data.status == 'ok') {
                   console.log(data,"user detail")
                   this.profileData = data.data
                   this.vendorData = data.data;
                   this.profileData.name = data.data.usr_first_name
                   this.profileData.mail = data.data.usr_email_address
                   this.profileData.phone = data.data.usr_mobile_no
                   this.profileData.ceritificate= data.data.ceritificates
                   this.id = data.data.user_id
                    this.events.publish('login:created', this.profileData.name, this.profileData.mail);
                   this.ShippingAddress = []
                   this.BillingAddress = []
  
                   for(let i = 0;i<data.data.shipping_address.length;i++){
                     data.data.shipping_address[i].isChecked = false
                        this.ShippingAddress.push(data.data.shipping_address[i])
                   }
  
                   for(let i = 0;i<data.data.billing_address.length;i++){
                     data.data.billing_address[i].isChecked = false
                        this.BillingAddress.push(data.data.billing_address[i])
                   }
                         this.alert.Loader.hide();
                    }
              else {
                  this.alert.Alert.alert(data.data)
                  return;
              }
          }, (error) => {
              this.alert.Loader.hide();
              this.alert.Alert.alert("Cannot Connect To Server")
              return;
          })
        })          
    }


    addUser() {

        this.navCtrl.push("AdduserCustPage",{
            data : this.profileData,
            id : this.id,
            "page":"user_info"
          })
     
  }




  fullView(value) {
      console.log(value)
      this.photoViewer.show(this.GST_path + value);
  }
  fullViews(value) {
      console.log(value)
      this.photoViewer.show(this.Food_certi + value);
  }
  fullViewCat(value) {
      console.log(value)
      this.photoViewer.show(this.PAN_path + value);
  }
  gstPath(val) {
      let data = this.GST_path + val;
      return data
  }

  panPath(val) {
    let data = this.PAN_path + val;
    return data
}
  otherCertificatePath(val) {
      let data = this.Food_certi + val;
      return data
  }
  // deleteadditional() {
  //     let AddUser = []
  //     let temp = {
  //         addi_cat_id: this.vendorData.addi_user.cat_usr_id,
  //         name: '',
  //         email: '',
  //         phone: '',
  //         addprofile: ''
  //     }
  //     AddUser.push(temp)
  //     let item = localStorage.getItem("tokens")
  //     this.anArrays = this.vendorData.delivery_days
  //     if (this.anArrays.length > 0) {
  //         for (let i = 0; i < this.anArrays.length; i++) {
  //             if (!this.anArrays[i].fromtime.hour) {
  //                 if (this.anArrays[i].fromtime) {
  //                     let hour = this.anArrays[i].fromtime.substring(0, 2)
  //                     let minute = this.anArrays[i].fromtime.substring(3, 5)
  //                     let data = {
  //                         hour: '',
  //                         minute: ''
  //                     }
  //                     data.hour = hour
  //                     data.minute = minute
  //                     this.anArrays[i].fromtime = data
  //                 }
  //             }
  //             if (!this.anArrays[i].totime.hour) {
  //                 if (this.anArrays[i].totime) {
  //                     let hour = this.anArrays[i].totime.substring(0, 2)
  //                     let minute = this.anArrays[i].totime.substring(3, 5)
  //                     let data = {
  //                         hour: '',
  //                         minute: ''
  //                     }
  //                     data.hour = hour
  //                     data.minute = minute
  //                     this.anArrays[i].totime = data
  //                 }
  //             }
  //         }
  //     }
  //     let datas = {
  //         "catt_first_name": this.vendorData.catt_first_name,
  //         "catt_email_address": this.vendorData.catt_email_address,
  //         "catt_mobile_no": this.vendorData.catt_mobile_no,
  //         // "catt_sur_name": this.vendorData.catt_sur_name,
  //         // "DeliveryTime_from": this.vendorData.catt_delivery_fromtime,
  //         // "DeliveryTime_to": this.vendorData.catt_delivery_totime,
  //         // "catt_event_type": this.vendorData.catt_event_type,
  //         "catt_min_order": this.vendorData.catt_min_order,
  //         "catt_max_order": this.vendorData.catt_max_order,
  //         "catt_lead_time": this.vendorData.catt_lead_time,
  //         // "catt_score": this.vendorData.catt_score,
  //         "catt_gst_certificate": this.vendorData.catt_gst_certificate,
  //         // "catt_food_certificate": this.vendorData.catt_food_certificate,
  //         // "newuser": this.vendorData.newuser,
  //         "catt_address": this.vendorData.catt_address,
  //         "catt_id": this.vendorData.catt_id,
  //         // "caterer_group" : this.vendorData.caterer_group,
  //         "catt_add_users": AddUser,
  //         "meal_types": this.vendorData.meal_types,
  //         "catt_catering_name": this.vendorData.catt_caterering_name,
  //         // "catt_catering_info":this.vendorData.catt_caterering_info,
  //         "catt_delivery_pincode": this.vendorData.cust,
  //         "delivery_day": this.anArrays || [],
  //         "catt_upload_file": this.vendorData.catt_upload_file || []
  //     }
  //     console.log("after add user", datas)
  //     this.user.updateProfile(item, datas).subscribe((data: any) => {
  //         console.log(data)
  //         if (data.status == 'ok') {
  //             this.zone.run(() => {
  //                 let token: any = localStorage.getItem("tokens")
  //                 let data: any = {
  //                     token: '',
  //                 }
  //                 data.token = token;
  //                 this.user.getCaterer(data).subscribe((data: any) => {
  //                      this.events.publish('audit:status', data.data.audit_deactive);
  //                     this.vendorData = data.data;
  //                     debugger
  //                     if (this.vendorData.is_updated == 1) {
  //                         this.Golive = true
  //                         this.show = true
  //                         this.status = "GO LIVE"
  //                     } else if (this.vendorData.is_updated == 2) {
  //                         this.Golive = false
  //                         this.show = true
  //                         this.status = "PENDING"
  //                     } else if (this.vendorData.is_updated == 0) {
  //                         this.Golive = false
  //                         this.status = ""
  //                         this.show = false
  //                     }
  //                     if (this.vendorData.pincode_changed == true) {
  //                         this.Address = this.vendorData.old_pincode;
  //                     } else {
  //                         this.Address = this.vendorData.new_pincode;
  //                     }
  //                     this.DeliveryDays = this.vendorData.delivery_days
  //                     this.Holidays = this.vendorData.holidays
  //                     this.Certificate = this.vendorData.ceritificates
  //                     let temp = []
  //                     this.showLess = false
  //                     this.hasmorethanfifteen = false
  //                     this.hasmorethanfive = false
  //                     if (this.Address.length > 0) {
  //                         if (this.Address.length > 5) {
  //                             for (let i = 0; i < 5; i++) {
  //                                 temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
  //                             }
  //                             this.pincodes = temp
  //                             this.hasmorethanfive = true
  //                         } else {
  //                             for (let i = 0; i < this.Address.length; i++) {
  //                                 temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
  //                             }
  //                             this.pincodes = temp
  //                         }
  //                     }
  //                     this.alert.Toast.show("Additional Deleted Successfully")
  //                     console.log(this.vendorData, "see")
  //                     this.alert.Loader.hide();
  //                 }, (error) => {
  //                     this.alert.Loader.hide();
  //                     this.alert.Alert.alert("Cannot Connect To Server")
  //                     return;
  //                 })
  //             })
  //         } else {
  //             this.alert.Loader.hide()
  //             this.alert.Alert.alert(data.data)
  //             return;
  //         }
  //     }, (error) => {
  //         this.alert.Loader.hide();
  //         this.alert.Alert.alert("Cannot Connect To Server")
  //         return;
  //     })
  // }
 
  showDelete() {
      if (this.certifi_delete == false) {
          this.certifi_delete = true
      } else {
          this.certifi_delete = false
      }
  }
  refreshVendor() {
    this.getDetails()
  }
  // deleteFood() {
  //     let token = localStorage.getItem("tokens")
  //     let data: any = {
  //         ceritificate: "certificate",
  //         token: token
  //     }
  //     console.log(data)
  //     // this.alert.presentDelete().then((res) => { 
  //     this.user.deleteCertificate(data).subscribe((data: any) => {
  //         if (data.status == "ok") {
  //             this.alert.Loader.hide();
  //             this.alert.Toast.show("Deleted Successfully")
  //             this.certifi_delete = false
  //             this.refreshVendor()
  //         } else {
  //             this.alert.Loader.hide()
  //             this.alert.Alert.alert(data.data)
  //             return
  //         }
  //     }, (error) => {
  //         this.alert.Loader.hide();
  //         this.alert.Alert.alert("Cannot Connect To Server")
  //         return;
  //     })
  //     // }, err => {
  //     //  this.alert.Loader.hide();
  //     //       console.log(err)
  //     this.certifi_delete = false
  //     // })
  // }
  deleteGst() {
      let token = localStorage.getItem("tokens")
      let data: any = {
          ceritificate: "gst",
          token: token
      }
      console.log(data)
      // this.alert.presentDelete().then((res) => { 
      this.user.deleteOtherCertificates(data).subscribe((data: any) => {
          if (data.status == "ok") {
              this.alert.Loader.hide();
              this.alert.Toast.show("Deleted Successfully")
              this.certifi_delete = false
              this.refreshVendor()
          } else {
              this.alert.Loader.hide()
              this.alert.Alert.alert(data.data)
              return
          }
      }, (error) => {
          this.alert.Loader.hide();
          this.alert.Alert.alert("Cannot Connect To Server")
          return;
      })
      //   }, err => {
      // this.alert.Loader.hide();
      //      console.log(err)
      this.certifi_delete = false
      // }) 
  }

  editCater() {
      // this.navCtrl.push("CateringInfoEditPage", {
      //     data: this.vendorData,
      // })
  }
  editAddedUser() {
      this.navCtrl.push("AdduserCustPage", {
          edit: true,
          data: this.vendorData,
          from: "vendor"
      })
  }
  editCertificate() {
      this.navCtrl.push("CertificateEditPage", {
          data: this.vendorData
      })
  }
  onChangeName() {
      console.log(this.vendorData)
  }
  addCate() {
      if (this.catge_flag == false) {
          this.catge_flag = true
      } else {
          this.catge_flag = false
          this.Category = ""
      }
  }



  editMenu() {
      this.navCtrl.push("MenugroupPage")
  }
  deleteGr() {
      if (this.ShowMinus == false) {
          this.ShowMinus = true;
      } else {
          this.ShowMinus = false
      }
  }
  scrollToBottom() {
      this.content.scrollToBottom();
  }
//   deleteFood() {
//     let token = localStorage.getItem("tokens")
//     let data: any = {
//         ceritificate: "food",
//         token: token
//     }
//     console.log(data)
//     // this.alert.presentDelete().then((res) => { 
//     this.user.deleteCertificate(data).subscribe((data: any) => {
//         if (data.status == "ok") {
//             this.alert.Loader.hide();
//             this.alert.Toast.show("Deleted Successfully")
//             this.certifi_delete = false
//             this.refreshVendor()
//         } else {
//             this.alert.Loader.hide()
//             this.alert.Alert.alert(data.data)
//             return
//         }
//     }, (error) => {
//         this.alert.Loader.hide();
//         this.alert.Alert.alert("Cannot Connect To Server")
//         return;
//     })
//     // }, err => {
//     //  this.alert.Loader.hide();
//     //       console.log(err)
//     this.certifi_delete = false
//     // })
// }
  public FoodCertificateDelete() {
      this.alertPresented = true
      localStorage.setItem("alert", this.alertPresented);
      let actionSheet = this.actionSheetCtrl.create({
          title: 'Food Certificate',
          buttons: [{
              text: 'Delete',
              icon: 'trash',
              handler: () => {
                  this.alertPresented = false
                  localStorage.setItem("alert", this.alertPresented);
                 // this.deleteFood()
              }
          }, {
              text: 'Cancel',
              icon: 'close',
              role: 'cancel',
              handler: () => {
                  this.alertPresented = false
                  localStorage.setItem("alert", this.alertPresented);
                  this.certifi_delete = false
              }
          }]
      });
      actionSheet.present();
  }
  public GSTCertificateDelete() {
      this.alertPresented = true
      localStorage.setItem("alert", this.alertPresented);
      let actionSheet = this.actionSheetCtrl.create({
          title: 'GST Certificate',
          buttons: [{
              text: 'Delete',
              icon: 'trash',
              handler: () => {
                  this.alertPresented = false
                  localStorage.setItem("alert", this.alertPresented);
                  this.deleteGst()
              }
          }, {
              text: 'Cancel',
              icon: 'close',
              role: 'cancel',
              handler: () => {
                  this.alertPresented = false
                  localStorage.setItem("alert", this.alertPresented);
                  this.certifi_delete = false
              }
          }],
      });
      actionSheet.present();
  }
  public DeleteAdditionalUser() {
      this.alertPresented = true
      localStorage.setItem("alert", this.alertPresented);
      let actionSheet = this.actionSheetCtrl.create({
          title: 'Additional User',
          buttons: [{
              text: 'Delete',
              icon: 'trash',
              handler: () => {
                  this.alertPresented = false
                  localStorage.setItem("alert", this.alertPresented);
                 // this.deleteadditional()
              }
          }, {
              text: 'Cancel',
              icon: 'close',
              role: 'cancel',
              handler: () => {
                  this.alertPresented = false
                  localStorage.setItem("alert", this.alertPresented);
              }
          }],
      });
      actionSheet.present();
  }


 
    ionViewDidEnter(){
      this.getDetails()
     // this.alert.Loader.show("Loading..")
      this.Food_certi = this.api.Food_certificate
      this.GST_path = this.api.GST_certificate
      this.PAN_path = this.api.PAN_certificate
 
    }
}
