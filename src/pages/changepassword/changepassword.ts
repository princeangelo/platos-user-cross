import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { LoadingController, ToastController } from 'ionic-angular';
// import { MyProfilePage } from '../my-profile/my-profile'
/**
 * Generated class for the ChangepasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
})
export class ChangepasswordPage {
token:any

  data:any = {
  current_password:'',    
  new_password:'',
  confirm_password:'',
  }

 passwordType: string = 'password';
 passwordIcon: string = 'eye-off';

 passwordType2: string = 'password';
 passwordIcon2: string = 'eye-off';

 passwordType3: string = 'password';
 passwordIcon3: string = 'eye-off';

  error:any={
  current_password:'',
  new_password:'',
  confirm_password:''
  }

  id:any

  constructor(public navCtrl: NavController, public navParams: NavParams,
      public loadingCtrl: LoadingController,
  public toastCtrl: ToastController,
  public user: UserProvider,
  public alert: AlertProvider) {
      
     this.id = this.navParams.get("id")
  }



  logForm() {
    console.log(this.data)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepasswordPage');
  }


  confirm(){

    if(this.data.current_password == ''){
       this.error.current_password = "Please Enter the Current Password"
       return
     }

     if(this.data.new_password == ''){
       this.error.new_password = "Please Enter the New Password"
       return
     }

     if(this.data.new_password.length < 6)
      {
       this.error.new_password = "Min Password Length Should Be 6"
       return
      }

     if(this.data.confirm_password == ''){
       this.error.confirm_password = "Please Enter Confirm Password"
       return
     }

     if(this.data.confirm_password.length < 6){
       this.error.confirm_password = "Min Password Length Should Be 6"
       return
     }

     // var n = this.data.new_password.localeCompare(this.data.con_password);

     // if(n != 0)
     // {
     //   this.error.con_pwd = "Password Does Not Match"
     //   return
     // }
    

    this.user.ChangePassword(this.id,this.data).subscribe((data: any) => {
      console.log(data)
            if (data.status == 'ok') {
              this.alert.Toast.show("Password Changed Successfully")
                this.navCtrl.setRoot('MyProfilePage')
              }
            else {
                 this.alert.Loader.hide();
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {

            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })

  }

     showHide() {
     this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
     this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
 

      showHides() {
     this.passwordType2 = this.passwordType2 === 'text' ? 'password' : 'text';
     this.passwordIcon2 = this.passwordIcon2 === 'eye-off' ? 'eye' : 'eye-off';
   }


      showHidess() {
     this.passwordType3 = this.passwordType3 === 'text' ? 'password' : 'text';
     this.passwordIcon3 = this.passwordIcon3 === 'eye-off' ? 'eye' : 'eye-off';
   }
   


   onChangeTime(value:any){
    this.error.new_password = "";
   }

   
   onChangeTimes(value:any){
     this.error.confirm_password = "";
   }

     onChangeCurPass(value:any){
     this.error.current_password = "";
   }

}
