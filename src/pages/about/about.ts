import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { NgZone } from '@angular/core';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
Cart_Count:any

  constructor(public api :Api,public user: UserProvider,public alert : AlertProvider,public events: Events,public _zone: NgZone,public navCtrl: NavController, public navParams: NavParams) {
   events.subscribe('cart:refresh', (count) => {
    this._zone.run(() => { 
    this.Cart_Count = count;
    console.log("Got data from cart")
  })
 })   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
    this.getCount();
  }

      getCount(){
 this._zone.run(() =>   {
  let token = localStorage.getItem("token")
         this.user.Notification(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               // this.alert.Toast.show("Added to Cart")
               console.log(data,"notification data")
               this.Cart_Count = data.data.cart_count

              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    })
 
}

buy(){
  this.navCtrl.push("MyCartPage")
}

}
