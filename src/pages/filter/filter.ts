import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { NgZone } from '@angular/core';
import { Events } from 'ionic-angular';
/**
/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
data:any = {
	pin:'',
	date:'',
	time:'',
	range_id:'',
  guest:'',
  meal_id:'',
  cuisi_id:'',
  packi_id:'',
  vegie:''

}
days:any;

budget:any = [{"id":1,"range":"Below 500"},
              {"id":2,"range":"Below 1000"},    
              {"id":3,"range":"Below 5000"},
              {"id":4,"range":"Below 10000"},
              {"id":5,"range":"Below 15000"}]

mealTypes:any
cusineTypes:any
pinTypes:any
packTypes:any
checkTypes:any=[{"id":1,"name":"Most Booked"},
{"id":2,"name":"Latest Caterers"}]

  constructor(public api :Api,public user: UserProvider,public alert : AlertProvider,public events: Events,public _zone: NgZone,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.getMealTypes()
    this.getCusinList()
    this.getPinList()
    this.getPackList()
    console.log('ionViewDidLoad FilterPage');
  }

  getMealTypes(){
  	        this.user.getMealTypes().subscribe((data: any) => {
            if (data.status == 'ok') {
              this.mealTypes = data.data;
              for(let j = 0;j<this.mealTypes.length;j++){
                console.log(data.data)
                this.mealTypes[j].selected = false
              }
                   this.alert.Loader.hide();
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
            }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  }
  getCusinList(){
 
    this._zone.run(() =>  {
     // this.alert.Loader.show("Loading")
    
           this.user.getCusineTypes().subscribe((data: any) => {
               if (data.status == 'ok') {
                 this.cusineTypes = data.data;
                 console.log(JSON.stringify(data.data));

                 for(let j = 0;j<this.cusineTypes.length;j++){
               
                   this.cusineTypes[j].selected = false
                 }
                     // this.alert.Loader.hide();
                 }
               else {
                   this.alert.Alert.alert(data.data)
                   return;
               }
           }, (error) => {
               this.alert.Loader.hide();
               this.alert.Alert.alert("Cannot Connect To Server")
               return;
           })
    
    })
  }
  getPinList(){
 
    this._zone.run(() =>  {
     // this.alert.Loader.show("Loading")
    
           this.user.getPinCod().subscribe((data: any) => {
               if (data.status == 'ok') {
                 this.pinTypes = data.data;
                 console.log(JSON.stringify(data.data));

                 for(let j = 0;j<this.pinTypes.length;j++){
               
                   this.pinTypes[j].selected = false
                 }
                     // this.alert.Loader.hide();
                 }
               else {
                   this.alert.Alert.alert(data.data)
                   return;
               }
           }, (error) => {
               this.alert.Loader.hide();
               this.alert.Alert.alert("Cannot Connect To Server")
               return;
           })
    
    })
  }
  getPackList(){
 
    this._zone.run(() =>  {
     // this.alert.Loader.show("Loading")
    
           this.user.getPackTypes().subscribe((data: any) => {
               if (data.status == 'ok') {
                 this.packTypes = data.data;
                 console.log(JSON.stringify(data.data));

                 for(let j = 0;j<this.packTypes.length;j++){
               
                   this.packTypes[j].selected = false
                 }
                     // this.alert.Loader.hide();
                 }
               else {
                   this.alert.Alert.alert(data.data)
                   return;
               }
           }, (error) => {
               this.alert.Loader.hide();
               this.alert.Alert.alert("Cannot Connect To Server")
               return;
           })
    
    })
  }
 // amPmString:any;
 //  amPmString=this.data.time;
  
  FilterData(){

    let currentDate = new Date(this.data.date);
    let weekdays = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
    this.days = weekdays[currentDate.getDay()];
    let day1: any;
    
           if(this.days=='monday'){
             day1=1;
           }else if(this.days=='tuesday'){
            day1=2;
           }else if(this.days=='wednesday'){
            day1=3;
           }else if(this.days=='thursday'){
            day1=4;
           }else if(this.days=='friday'){
            day1=5;
          }else if(this.days=='saturday'){
            day1=6;
          }else if(this.days=='sunday'){
            day1=7; 
          }
           
    console.log(this.days)
    let data1 = {
      "page_no": 1,
      "page_count": 10,
      "meal_type": this.data.meal_id,
      "veg_nonveg": this.data.vegie,
      "cuis_type": this.data.cuisi_id,   
      "pack_type": this.data.packi_id, 
      "place": this.data.pin,
      "Delivery_date": day1,
      "Delivery_time":"10:30",  
      "PerPerson": this.data.range_id,    
      "No_of_guest":  this.data.guest,
      "caterer_option":"Most Booked"
                                                             
} 

this.user.Filter_submit(data1).subscribe((data: any) => {
  console.log(data)
        if (data.status == 'ok') {
            this.alert.Loader.hide();
            // this.alert.Toast.show("Event Catering Added Successfully");
            // this.navCtrl.setRoot('DashboardPage')
                  
                  // this.createUser(data.data.catt_first_name)
                  // this.navCtrl.setRoot(MainPage)
               // }
          }
        else {
            this.alert.Loader.hide();
            this.alert.Alert.alert(data.data)
            return;
        }
    }, (error) => {

        this.alert.Loader.hide();
        this.alert.Alert.alert("Cannot Connect To Server")
        return;
    })

  // let temp = [];
  // for(var i = 0; i < this.mealTypes.length; ++i) {
  //   if(this.mealTypes[i].selected == true){
  //     temp.push(this.mealTypes[i].mealtype_id)
  //   }  
  //   }  
  // console.log(temp.join(),"mealtipe")
   console.log(data1)
  }



}
