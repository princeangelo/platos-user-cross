import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { NgZone } from '@angular/core';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';


@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

forarray: any=[
{src: 'assets/imgs/home/computer.png', name: 'Order Online', para: 'Order delicious catering online in just 5 minutes.'},
{src: 'assets/imgs/home/calendar.png', name: 'Event Catering', para: 'Book delicious catering for your event online.'},
{src: 'assets/imgs/home/contract.png', name: 'Contract Catering', para: 'Lorem Ipsum is simply dummy text of the printing and typesetting.'}
];

Cart_Count:any = 0; 
profileData:any = {
  name:'',
  mail:'',
  phone:''
}
user_id:any;

  constructor(public api :Api,public user: UserProvider,public alert : AlertProvider,public events: Events,public _zone: NgZone,public navCtrl: NavController, public navParams: NavParams) {
    this.getDetails()
    events.subscribe('cart:refresh', (count) => {
    this._zone.run(() => { 
    this.Cart_Count = count;
    console.log("Got data from cart")
  })
  })
    
  }

  ionViewDidLoad() {
    this.getCount()
    console.log('ionViewDidLoad DashboardPage');
  }


      getCount(){
 this._zone.run(() =>   {
  let token = localStorage.getItem("token")
         this.user.Notification(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               // this.alert.Toast.show("Added to Cart")
               console.log(data,"notification data")
               this.Cart_Count = data.data.cart_count

              }
            else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    })
 
}

buy(){
  this.navCtrl.push("MyCartPage")
}

openPage(value){
if(value == 0){
this.navCtrl.push('OrderPage')
}
else if(value == 1){
  this.navCtrl.push('EventCateringPage',{user_id:this.user_id})
}else if(value == 2){
  this.navCtrl.push('ContractCateringPage',{user_id:this.user_id})
}
else{
  // this.navCtrl.setRoot('')
}
}


 getDetails(){
    this.alert.Loader.show("Loading..")
     this._zone.run(() =>   {
        let token = localStorage.getItem("token")
                this.user.UserDetails(token).subscribe((data: any) => {
                if (data.status == 'ok') {
                 console.log(data,"user detail")
                 this.profileData = data.data
                 this.user_id=data.data.user_id;


                 this.profileData.name = data.data.usr_first_name
                 this.profileData.mail = data.data.usr_email_address
                 this.profileData.phone = data.data.usr_mobile_no

   this.events.publish('login:created', this.profileData.name, this.profileData.mail);
                var len = this.profileData.name.length
                this.profileData.name = this.profileData.name.substring(0,13)
                
                if(len > 13){
                this.profileData.name = this.profileData.name + "..."
                }

                       this.alert.Loader.hide();
                  }
            else {
                 this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
      })          
  }

}
