import { Component } from '@angular/core';
import { IonicPage, MenuController,NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { Events } from 'ionic-angular';
// import {
//     MainPage
// } from '../../pages/pages';
import { DashboardPage } from '../../pages/dashboard/dashboard';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

data:any = {
  username:'',
  password:'',
  is_mob_login:true
}

error:any = {
  username:'',
  password:''
}

 passwordType: string = 'password';
 passwordIcon: string = 'eye-off';

  constructor(public events: Events,public navCtrl: NavController, public navParams: NavParams,  public user: UserProvider,
  public alert: AlertProvider,public menu: MenuController) {
  this.menu.enable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  createUser(user) {
  console.log('User created!',user)
  this.events.publish('user:created', user, Date.now());
}

  

  login(){

// this.menu.enable(true)

// this.navCtrl.setRoot('DashboardPage')
         if (!this.data.username) {
            // this.alert.Alert.alert("Please Enter The Email");
            this.error.username = "Please Enter The Email"
            return
        }

        if(this.data.username){
           var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

              if (reg.test(this.data.username) == false) 
              {
                  console.log("Invalid Email Address");
                  this.error.username = "Invalid Mail Address"
                  return
              }
        }

         if (!this.data.password) {
            // this.alert.Alert.alert("Please Enter The Email");
            this.error.password = "Please Enter The Password"
            return
        }


         if (this.data.password.length < 6) {
            // this.alert.Alert.alert("Please Enter The Email");
            this.error.password = "Min Password Length Should be 6"
            return
        }





    this.alert.Loader.show("Loading..")
    console.log(this.data)

        this.user.toLogin(this.data).subscribe((data: any) => {
      console.log(data)
            if (data.status == 'ok') {
                this.alert.Loader.hide();
                this.menu.enable(true)
                
                  
                    if(data.data){
                       localStorage.setItem("user_name",  data.data.usr_first_name)
                       localStorage.setItem("user_email",  data.data.usr_email_address)
                       localStorage.setItem("token", data.data.token);
                       // localStorage.setItem("pic",data.data.catt_profile_pic)
                       let name = data.data.usr_first_name
                       let mail = data.data.usr_email_address
                       // let profile = data.data.catt_profile_pic
                       // let admin = data.data.caterer_admin
                        this.events.publish('login:created', name, mail);
                        this.navCtrl.setRoot('DashboardPage')
                         // this.events.publish('profilepic:created', profile);
                          // this.events.publish('isAdmin:created', admin);
                    } 
                  

                     
                      

                      
                      // this.createUser(data.data.catt_first_name)
                      // this.navCtrl.setRoot(MainPage)
                   // }
              }
            else {
                this.alert.Loader.hide();
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {

            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  }

  signup(){
    this.navCtrl.push("SignupPage")
  }

  forgot(){
    this.navCtrl.push("ForgotPage")
  }


   showHide() {
     this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
     this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
 }


 // onChangeTime(value:any){
 //  console.log("value",value)
 //  this.error.username = ''
 // }

 onChangeTime(value){


        if(value == '')
        {
          this.error.username = "Please Enter the Email ID"
          return
        }
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(value) == false) 
        {
            console.log("Invalid Email Address");
            this.error.username = "Invalid Mail Address"
           
        }
        else{
          console.log("Valid Address")
          this.error.username = ''
        }
}

onChangePass(){
  this.error.password = ""
}


   moveFocus(nextElement) {
        nextElement.setFocus();
    }

}
