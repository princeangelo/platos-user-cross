import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { NgZone } from '@angular/core';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';

@IonicPage()
@Component({
  selector: 'page-my-profile',
  templateUrl: 'my-profile.html',
})
export class MyProfilePage {
ShippingAddress:any =[]
BillingAddress:any = []
profileData:any = {
  name:'',
  phone:'',
  mail:''
}
Cart_Count:any = 0
id:any
show_add:boolean=false;

  constructor(public api :Api,public user: UserProvider,public alert : AlertProvider,public events: Events,public _zone: NgZone,public navCtrl: NavController, public navParams: NavParams) {


    events.subscribe('cart:refresh', (count) => {
    this._zone.run(() => { 
    this.Cart_Count = count;
    console.log("Got data from cart")
  })
 })   
  }

  ionViewDidLoad() {
    this.getCount()
  }

  editProfile(){
  	this.navCtrl.push("ProfileEditPage",{
      data : this.profileData,
      id : this.id,
      "page":"my_prof"
    })
  }

        getCount(){
 this._zone.run(() =>   {
  let token = localStorage.getItem("token")
         this.user.Notification(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               // this.alert.Toast.show("Added to Cart")
               console.log(data,"notification data")
               this.Cart_Count = data.data.cart_count

              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    })
 
}

// AddDelivery(){
//  this.navCtrl.push("AddShipaddressPage",{
//    from:"delivery"
//  })
// }

// AddBilling(){
//  this.navCtrl.push("AddShipaddressPage",{
//    from:"billing"
//  })
// }

ChangePass(){
  this.navCtrl.push("ChangepasswordPage",{
    id: this.id
  })
}

  getDetails(){
    this.alert.Loader.show("Loading..")
     this._zone.run(() =>   {
        let token = localStorage.getItem("token")
                this.user.UserDetails(token).subscribe((data: any) => {
                if (data.status == 'ok') {
                 console.log(data,"user detail")
                 this.profileData = data.data

                 this.profileData.name = data.data.usr_first_name
                 this.profileData.mail = data.data.usr_email_address
                 this.profileData.phone = data.data.usr_mobile_no
                 this.id = data.data.user_id
                  this.events.publish('login:created', this.profileData.name, this.profileData.mail);
                 this.ShippingAddress = []
                 this.BillingAddress = []

                 for(let i = 0;i<data.data.shipping_address.length;i++){
                   data.data.shipping_address[i].isChecked = false
                      this.ShippingAddress.push(data.data.shipping_address[i])
                 }

                 for(let i = 0;i<data.data.billing_address.length;i++){
                   data.data.billing_address[i].isChecked = false
                      this.BillingAddress.push(data.data.billing_address[i])
                 }
                       this.alert.Loader.hide();
                  }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
      })          
  }

  ionViewDidEnter(){
    this.getDetails()
  }

}
