import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdduserCustPage } from './adduser-cust';

@NgModule({
  declarations: [
    AdduserCustPage,
  ],
  imports: [
    IonicPageModule.forChild(AdduserCustPage),
  ],
})
export class AdduserCustPageModule {}
