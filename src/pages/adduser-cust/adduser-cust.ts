import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Events, ToastController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';

/**
 * Generated class for the AdduserCustPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adduser-cust',
  templateUrl: 'adduser-cust.html',
})
export class AdduserCustPage {
  data:any = {
    usr_first_name:'',
    usr_email_address:'',
    usr_mobile_no:'',
    user_id:'',
      } 
    
      fromEdit:any
    
      error:any = {
    first_name:'',
    mobile_no:'',
    email_address:'',
      } 
      tit:any
    getProfileData:any
    
    id:any
    page:any
      constructor(public toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams,public user: UserProvider,public alert: AlertProvider) {
     this.getProfileData = this.navParams.get("data")
     this.id = this.navParams.get("id")
     this.page = this.navParams.get("page")
     console.log(this.getProfileData)
     this.data.usr_first_name = this.getProfileData.name;
     this.data.usr_email_address = this.getProfileData.mail;
     this.data.usr_mobile_no = this.getProfileData.phone;
     this.data.user_id = this.getProfileData.user_id
    

     this.getProfileData = this.navParams.get("data")
     this.id = this.navParams.get("id")
     this.fromEdit = this.navParams.get("page")
  
     if (this.fromEdit == "user_info") {
         this.tit = "Add User Edit"
         this.data.name = this.getProfileData.addi_user.add_user_name;
         this.data.email = this.getProfileData.addi_user.add_user_email;
         this.data.phone = this.getProfileData.addi_user.add_user_mobile;
         this.data.user = this.getProfileData.addi_user.add_user_id;

         console.log(this.data.phone)
     } else {
         this.tit = "Add User"
     }


      }
    
      ionViewDidLoad() {
        console.log('ionViewDidLoad ProfileEditPage');
      }
    
      onChangeName(){
      this.error.first_name = "";
    }
    
    // onChangeSurName(){
    //   this.error.sur_name = "";
    // }
    
    onChangenumber(){
      this.error.mobile_no = "";
    }
    
    
    onChangeemail(value){
         if(value == '')
            {
              this.error.email_address = "Please Enter the Email ID"
              return
            }
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    
            if (reg.test(value) == false) 
            {
                console.log("Invalid Email Address");
                this.error.email_address = "Invalid Mail Address" 
            }
            else{
              console.log("Valid Address")
              this.error.email_address = ''
            }
    }
    
    
    updateProfile(){
    
    
      if(this.data.usr_first_name == ""){
      this.error.first_name = "Please Enter the Firstname"
      return
      }
    
      //  if(this.data.catt_sur_name == ""){
      // this.error.sur_name = "Please Enter the Surname"
      // return
      // }
    
       if(this.data.usr_email_address == ""){
      this.error.email_address = "Please Enter the EmailId"
      return
      }
    
      if(this.data.usr_email_address){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    
            if (reg.test(this.data.usr_email_address) == false) 
            {
                console.log("Invalid Email Address");
                this.error.email_address = "Invalid Mail Address"
                return 
            }
            else{
              console.log("Valid Address")
              this.error.email_address = ''
            }
      }
    
       if(this.data.usr_mobile_no == ""){
      this.error.mobile_no = "Please Enter the Mobile Number"
      return
      }
    
         if(this.data.usr_mobile_no.length < 10){
      this.error.mobile_no = "Invalid Mobile Number"
      return
      }
    let values=[{
     "add_user_name" : this.data.name ,
      "add_user_email": this.data.email ,
     "add_user_mobile" :this.data.phone ,
     "add_user_id" :this.data.user 
    }]
    let data={
      "add_user":values
    }
    
    console.log(this.data,this.id)
    
               // this.alert.Loader.show("Loading..")
             
            console.log(this.user.UpdateProfile(this.getProfileData.user_id,this.data))
        this.user.UpdateProfile(this.getProfileData.user_id,data).subscribe((data: any) => {
          console.log(data)
          if(data.status == 'ok'){
            this.alert.Loader.hide()
            this.presentToast("Profile Updated Successfully")
            // if( this.page == "my_prof"){
            // this.navCtrl.setRoot('MyProfilePage')
            // }else{
              this.navCtrl.setRoot('UserinfoPage')
          //  }
          }
           else{
            this.alert.Loader.hide()
            this.alert.Alert.alert(data.data)
            return;
          }
    
    
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
    
    
    }
    
        presentToast(msg) {
            let toast = this.toastCtrl.create({
                message: msg,
                duration: 3000,
                position: 'bottom'
            });
            toast.onDidDismiss(() => {
                console.log('Dismissed toast');
            });
            toast.present();
        }
    
    
    }
    