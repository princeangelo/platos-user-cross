import { Component } from '@angular/core';
import { IonicPage, MenuController,NavController, NavParams } from 'ionic-angular';
import { LoadingController, ToastController } from 'ionic-angular';
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
// import { Camera, CameraOptions } from '@ionic-native/camera';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { DomSanitizer,SafeUrl } from '@angular/platform-browser';
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {


  data:any = {
  fullname:'',
  phoneNo:'',
  email:'',
  pass:'',
  confPass:''
  }


  error:any ={
  fullname:'',
phoneNo:'',
email:'',
pass:'',
confPass:''
  }


   passwordType: string = 'password';
 passwordIcon: string = 'eye-off';

passwordTypes: string = 'password';
passwordIcons:string = 'eye-off';



  constructor(public navCtrl: NavController, public navParams: NavParams, 
  //  private transfer: FileTransfer,
  // private camera: Camera,
  public loadingCtrl: LoadingController,
  public toastCtrl: ToastController,
  public user: UserProvider,
  public alert: AlertProvider,
  private _DomSanitizer: DomSanitizer,
  public menu: MenuController) {

   this.menu.enable(false);
   
  }



   showHide() {
     this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
     this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
 }

 show(){
    this.passwordTypes = this.passwordTypes === 'text' ? 'password' : 'text';
     this.passwordIcons = this.passwordIcons === 'eye-off' ? 'eye' : 'eye-off';
 }




  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

       randomFileName() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var n = new Date()
        var date = n.toISOString();
        return text + "-" + date + ".png";
    }






onChangeName(){
  this.error.fullname = "";
}


onChangenumber(){
  this.error.phoneNo = "";
}

onChangePass(){
  this.error.pass = "";
}

onChangeConfPass(){
  this.error.confPass = "";
}


   moveFocus(nextElement) {
        nextElement.setFocus();
    }


onChangeemail(value){
     if(value == '')
        {
          this.error.email = "Please Enter the Email ID"
          return
        }
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(value) == false) 
        {
            console.log("Invalid Email Address");
            this.error.email = "Invalid Mail Address" 
        }
        else{
          console.log("Valid Address")
          this.error.email = ''
        }
}


async  signup(){

    if(this.data.fullname == ''){
      this.error.fullname = "Please Enter the First Name"
      this.alert.Alert.alert("Please Fill the Mandatory Fields")
      return
    }


     if(this.data.email == ''){
      this.error.email = "Please Enter the Email Id"
      return
    }

    if(this.data.email){
       var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(this.data.email) == false) 
        {
            console.log("Invalid Email Address");
            this.error.email = "Invalid Mail Address" 
            return
        }
    }

        if(this.data.phoneNo == ''){
      this.error.phoneNo = "Please Enter the Mobile Number"
      return
    }

     if(this.data.phoneNo.length < 10){
      this.error.phoneNo = "Invalid Mobile Number"
      return
    }

        if(this.data.pass == ''){
      this.error.pass = "Please Enter the Password"
      return
    }

     if(this.data.pass.length < 6){
      this.error.pass = "Password Length Should be Atleast 6"
      return
    }

     if(this.data.confPass == ''){
      this.error.confPass = "Please Enter the Password"
      return
    }

     if(this.data.confPass.length < 6){
      this.error.confPass = "Password Length Should be Atleast 6"
      return
    }

    if(this.data.pass != this.data.confPass){
      this.error.confPass = "Password doesn't Match"
      return
    }



console.log(this.data)

 let data1 = {
                    "usr_first_name": this.data.fullname,
                    "usr_email_address": this.data.email,
                    "usr_mobile_no": this.data.phoneNo,
                    "usr_password": this.data.pass,                                                        
              } 

    this.user.toSignUp(data1).subscribe((data: any) => {
      console.log(data)
            if (data.status == 'ok') {
                this.navCtrl.setRoot('LoginPage')
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })

}






  
}
