// The page the user lands on after opening the app and without a session

export const FirstRunPage = 'LoginPage';
// export const FirstRunPage = 'CatererDetailPage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'DashboardPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = '';
export const Tab2Root = '';
export const Tab3Root = '';
export const Tab4Root = '';
export const Tab6Root = '';
export const Tab5Root = '';
