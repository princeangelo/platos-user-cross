import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
/**
 * Generated class for the ShippingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shipping',
  templateUrl: 'shipping.html',
})
export class ShippingPage {

data:any = {
date:'',
mealtype_id:'',
guestCount:'',
time:'',
billing:''
}

mealTypes:any 
UserDetail:any
ShippingAddress:any = []
BillingAddress:any = []

  constructor(public api :Api,public user: UserProvider,public alert : AlertProvider,public navCtrl: NavController, public navParams: NavParams) {
  this.ShippingAddress = this.navParams.get("shipaddress")

  this.BillingAddress = this.navParams.get("billaddress")
  }

  ionViewDidLoad() {
      this.user.getMealTypes().subscribe((data: any) => {
            if (data.status == 'ok') {
              this.mealTypes = data.data;
                   this.alert.Loader.hide();
                   let token = localStorage.getItem("token")
             this.user.UserDetails(token).subscribe((data: any) => {
		            if (data.status == 'ok') {
		             console.log(data,"user detail")
		             this.UserDetail = data.data;
		             // for(let i = 0;i<data.data.shipping_address.length;i++){
		             // 	data.data.shipping_address[i].isChecked = false
               //        this.ShippingAddress.push(data.data.shipping_address[i])
		             // } 
		                   this.alert.Loader.hide();
		              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  }

  CurrentDate(){
  	let b = new Date().toDateString()
  	let date = b.substring(4)
  	this.data.order_date = date
  	return date
  }

PayNow(){
let token = localStorage.getItem("token")
let temp = []

if(this.data.date == ""){
	this.alert.Alert.alert("Please select the event date")
	return
}
if(this.data.time == ""){
	this.alert.Alert.alert("Please select the event time")
	return
}

if(this.data.guestCount == ""){
	this.alert.Alert.alert("Please Enter the guest count")
	return
}

if(this.data.mealtype_id == ""){
  this.alert.Alert.alert("Please select the event type")
  return
}

// for(let i = 0; i < this.ShippingAddress.length;i++){
// if(this.ShippingAddress[i].isChecked == true){
//   temp.push(this.ShippingAddress[i])
// }
// }

// if(temp.length == 1){

let billaddress:any

if(this.BillingAddress == "same"){
  billaddress = this.ShippingAddress[0].ship_add_id
}
else{
  billaddress = this.BillingAddress[0].bill_add_id
}

  	let orders = {

          "delivery_date":this.data.date,
          "order_date":this.data.order_date,
          "event_type":this.data.mealtype_id,
          "noguest":this.data.guestCount,
          "delivery_time":this.data.time,
          "user_id":token,
          "shipping_address":this.ShippingAddress[0].ship_add_id,
          "billing_address":billaddress,
          "other_instruction": "extra onion",
          "is_recurring": false,
      }

  	console.log(this.data)
  	console.log(this.ShippingAddress)

  	             this.user.PlaceOrder(orders).subscribe((data: any) => {
		            if (data.status == 'ok') {
		            this.alert.Toast.show("Order Placed Successfully")	
		            this.navCtrl.setRoot("OrderPage")
		             } 
		                   
		              
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  	         // } 
  	         // else if(temp.length == 0){
  	         // 	this.alert.Alert.alert("Please Select the Delivery Address")
  	         // 	return
  	         // }
  	         // else if(temp.length > 1){
  	         // 	this.alert.Alert.alert("Please Select One Address")
  	         // 	return
  	         // }
  }

}
