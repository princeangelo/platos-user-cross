import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CatererInfoPage } from './caterer-info';

@NgModule({
  declarations: [
    CatererInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(CatererInfoPage),
  ],
})
export class CatererInfoPageModule {}
