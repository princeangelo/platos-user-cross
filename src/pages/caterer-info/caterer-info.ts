import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { Slides } from 'ionic-angular';
/**
 * Generated class for the CatererInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-caterer-info',
  templateUrl: 'caterer-info.html',
})
export class CatererInfoPage {

    @ViewChild('slides') slides: Slides;


cat_Detail:any 
Images:any = []
imgPath:any

 slidesPerView : number = 1;
 caterer_image:any 
 defaultImage:any = "assets/imgs/cater_banner.png"
 Cater_Img:any

  constructor(public platform:Platform,public api :Api,public navCtrl: NavController, public navParams: NavParams) {
  this.imgPath = this.api.Menu_image;
  this.cat_Detail = this.navParams.get("catt")
  console.log(this.cat_Detail)
  this.Cater_Img = this.api.filepath;
  this.caterer_image = this.cat_Detail.caterer_image

  if(this.caterer_image != '' && this.caterer_image != null){

    this.caterer_image = this.Cater_Img + this.caterer_image;
  }
  else{
    this.caterer_image = this.defaultImage
  }

  let temp = this.cat_Detail.menus
  let temp1 = []

  for(let i = 0 ; i < temp.length ; i++){
  	for(let j = 0;j < temp[i].images.length ; j++){

      temp1.push(temp[i].images[j].menu_imgname)
  	}
  }


  console.log(temp1)
this.Images = temp1;

  }

 ionViewDidLoad() {
    console.log('ionViewDidLoad HomepagePage');
        if(this.platform.width() > 1200) {
      this.slidesPerView = 4;
    }
 
    // On a desktop, and is wider than 768px
    else if(this.platform.width() > 768) {
      this.slidesPerView = 3;
    }
 
    // On a desktop, and is wider than 400px
    else if(this.platform.width() > 400) {
      this.slidesPerView = 3;
    }
 
    // On a desktop, and is wider than 319px
    else if(this.platform.width() > 319) {
      this.slidesPerView = 3;
    }
  }

  getImage(val){
  let data = this.imgPath + val;
  return data;
  } 

}
