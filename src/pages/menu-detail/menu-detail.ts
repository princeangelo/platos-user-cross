import { Component,ViewChild, } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {
    Slides
} from 'ionic-angular';
import {
    Api
} from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { NgZone } from '@angular/core';
import { Events } from 'ionic-angular';
/**
 * Generated class for the MenuDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-detail',
  templateUrl: 'menu-detail.html',
})
export class MenuDetailPage {
	 @ViewChild('slides') slides: Slides;
		MenuDetail:any
		Menu_Name:any
		SlideArr:any
        ImagePath:any
        isBegin: boolean
        isEnd: boolean
        defaultMenuPic = "assets/imgs/no_image.jpg"

cartItem:any
caterdata:any
minVal:any
maxVal:any
Cart_Count:any
  mi = "minus"
  pl = "plus"
Combo_List: any = []
items:any

  constructor(public events: Events,public _zone: NgZone,public alert: AlertProvider,public user: UserProvider,public api: Api,public navCtrl: NavController, public navParams: NavParams) {
   let detail = navParams.get('data');
   this.caterdata = navParams.get('caterData')
  this.minVal = parseInt(this.caterdata.catt_min_order)
  this.maxVal = parseInt(this.caterdata.catt_max_order)
   console.log(detail,"check here")

   this.MenuDetail = detail

   this.Menu_Name =  this.MenuDetail.menu_name
   this.SlideArr = this.MenuDetail.images;

   this.ImagePath = this.api.Menu_image;
  }

  ionViewWillEnter() {

     let comboItems = []
 let temp = []
 let menuTemp = []
    if (this.MenuDetail.is_combo) {
                if (this.MenuDetail.combos.length > 0) {
                    for (var i = 0; i < this.MenuDetail.combos.length; ++i) {
                        temp = []
                        menuTemp = []
                        for (var j = 0; j < this.MenuDetail.combos[i].combo_items.length; ++j) {
                            // temp.push(this.MenuDetail.combos[i].combo_items[j].menu_id)
                            menuTemp.push({
                              "menu_id" : this.MenuDetail.combos[i].combo_items[j].menu_id,
                              "menu_name" : this.MenuDetail.combos[i].combo_items[j].combo_menu.menu_name
                            })
                        }
                        comboItems.push({
                            "combo_item": '',
                            "combo_name": this.MenuDetail.combos[i].group_name,
                            "Items"     : menuTemp
                        })
                    }
                }
            }
            this.Combo_List = comboItems;
            console.log(this.Combo_List)


    this.checkCart()
    this.getCount()
  }

      getImage(value: any) {
        let image = this.ImagePath + value;
        return image;
    }
    next() {
        this.slides.slideNext();
    }
    prev() {
        this.slides.slidePrev();
    }

        backward() {
        console.log("Back Clicked", this.slides)
        this.slides.slidePrev();
        this.isBegin = this.slides._isBeginning
        this.isEnd = this.slides._isEnd
        console.log(this.isEnd, "end")
        console.log(this.isBegin, "begin")
    }

    buy(){
  this.navCtrl.push("MyCartPage",{
    min:this.minVal,
    max:this.maxVal
  })
}



    forward() {
        console.log("Forward Clicked")
        this.slides.slideNext();
        this.isBegin = this.slides._isBeginning
        this.isEnd = this.slides._isEnd
        console.log(this.isEnd, "end")
        console.log(this.isBegin, "begin")
    }



    checkInCart(menu,operator,num,id){


     let token = localStorage.getItem("token")
     debugger
     this.user.ViewCartItems(token).subscribe((data: any) => {
       let menuInCart = false
            if (data.status == 'ok') {
               console.log(data,"cart items")
               this.cartItem = data.data;
                if(this.cartItem.length > 0){
                 for(var j = 0 ; j < this.cartItem.length; j++){
                   if(menu.menu_id == this.cartItem[j].menu_id){
                     menuInCart = true
                   }
                 }

                 if(menuInCart == true){
                  if(operator == "plus"){
                    this.plus(num,id)
                  }
                  else{
                    this.minus(num,id)
                  }
                 }
                 else{
                     if(operator == "plus"){
                       this.plusCount(num)
                     }
                     else{
                       this.minusCount(num)
                     }
                 }
              }
              else{
                    if(operator == "plus"){
                       this.plusCount(num)
                     }
                     else{
                       this.minusCount(num)
                     }
              } 


              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })

}


  minusCount(num){
console.log(num)

  if(this.MenuDetail.number == this.minVal){
  console.log("reached 0")
  
  }else{
    this.MenuDetail.number = this.MenuDetail.number - 1;
  }
  }

 plusCount(num){
   if(this.MenuDetail.number == this.maxVal){
     console.log("reached 10")
   }
   else{
    this.MenuDetail.number = this.MenuDetail.number + 1;
   }
 }


     minus(num,id){

  if(this.MenuDetail.number == this.minVal){
  console.log("reached min value")
  }else{
    this.MenuDetail.number = this.MenuDetail.number - 1;
    let data:any = {
      quantity: '',
      item_id: ''
    }
    data.quantity = this.MenuDetail.number
    data.item_id = id
    console.log(data,"before api hit")
 this._zone.run(() =>   {    
    this.user.UpdateCartItem(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Cart Updated")
              
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  })
  }
  }

 plus(num,id){
   if(this.MenuDetail.number == this.maxVal){
     console.log("reached max value")
   }
   else{
    this.MenuDetail.number = this.MenuDetail.number + 1;
      let data:any = {
      quantity: '',
      item_id: ''
    }
    data.quantity = this.MenuDetail.number
    data.item_id = id
    console.log(data,"before api hit")
  this._zone.run(() =>   {   
    this.user.UpdateCartItem(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Cart Updated")
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  })
   }
 }


addtoCart(value:any){

if(value.is_combo == true){
  if(value.combos.length > 0){
   if(value.cart_id){
  let token = localStorage.getItem("token")
  let data:any = {
  catt_id: value.catt_id,
  menu_id:value.menu_id,
  price:value.per_person_cost,
  quantity:value.number,
  user_id:token
  }

console.log(value,"for add to cart")
           this.user.addToCart(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Added to Cart")
               value.incart = true
               this.checkCart()
               this.getCount()
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
   }
   else{
     console.log(this.Combo_List)
     for (var j = 0; j < this.Combo_List.length; ++j) {
        if(this.Combo_List[j].combo_item == ''){
         return this.alert.Alert.alert("Please Select the Combo")
        }
     }

     let token = localStorage.getItem("token")
let data:any = {
catt_id: value.catt_id,
menu_id:value.menu_id,
price:value.per_person_cost,
quantity:value.number,
user_id:token
}

console.log(value,"for add to cart")
           this.user.addToCart(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Added to Cart")
               value.incart = true
               this.checkCart()
               this.getCount()
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
   
   }
 }
}
else{

let token = localStorage.getItem("token")
let data:any = {
catt_id: value.catt_id,
menu_id:value.menu_id,
price:value.per_person_cost,
quantity:value.number,
user_id:token
}

console.log(value,"for add to cart")
           this.user.addToCart(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Added to Cart")
               value.incart = true
               this.checkCart()
               this.getCount()
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }       
}


getCount(){
 this._zone.run(() =>   {

  let token = localStorage.getItem("token")
         this.user.Notification(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               console.log(data,"notification data")
               this.Cart_Count = data.data.cart_count
               let count:boolean = this.Cart_Count
               this.events.publish('cart:refresh', count);
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    })
 
}



checkCart(){
   let token = localStorage.getItem("token")
     this.user.ViewCartItems(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               console.log(data,"cart items")
               this.cartItem = data.data;
                if(this.cartItem.length > 0){
                 for(var j = 0 ; j < this.cartItem.length; j++){
                   if(this.MenuDetail.menu_id == this.cartItem[j].menu_id){
                     this.MenuDetail.incart = true
                     this.MenuDetail.cart_id = this.cartItem[j].cart_id
                     this.MenuDetail.number = this.cartItem[j].quantity


           /************************* If Backend for combo is done do here uncomment below******************/



            //           if(this.cartItem[j].is_combo == true){
            //             if(this.cartItem[j].combos.length > 0){
            //                   let comboItems = []
            //                  let temp = []
            //                  let menuTemp = []

            //       for (var i = 0; i < this.cartItem[j].combos.length; ++i) {
            //             temp = []
            //             menuTemp = []
            //             for (var k = 0; k < this.cartItem[j].combos[i].Items.length; ++k) {
            //                 temp.push(this.cartItem[j].combos[i].Items[k].menu_id)
            //                 menuTemp.push({
            //                   "menu_id" : this.cartItem[j].combos[i].Items[k].menu_id,
            //                   "menu_name" : this.cartItem[j].combos[i].Items[k].combo_menu.menu_name
            //                 })
            //             }
            //             comboItems.push({
            //                 "combo_item": temp,
            //                 "combo_name": this.cartItem[j].combos[i].combo_name,
            //                 "Items"     : menuTemp
            //             })
            //         }

            //                     this.Combo_List = comboItems;
            // console.log(this.Combo_List,"new")

            //             }
            //           }
                   }
                    else{
                     if(this.MenuDetail.incart == false){ 
                     this.MenuDetail.incart = false
                     this.MenuDetail.cart_id = ''
                     this.MenuDetail.number = this.minVal
                     }
                   }
                 }
              } 
             }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
}



}
