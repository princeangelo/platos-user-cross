import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuDetailPage } from './menu-detail';
import { LongPressModule } from 'ionic-long-press';
@NgModule({
  declarations: [
    MenuDetailPage,
  ],
  imports: [
    LongPressModule,
    IonicPageModule.forChild(MenuDetailPage),
  ],
})
export class MenuDetailPageModule {}
