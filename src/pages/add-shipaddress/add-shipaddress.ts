import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { Directive, HostListener, ElementRef } from '@angular/core';
/**
 * Generated class for the AddShipaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-shipaddress',
  templateUrl: 'add-shipaddress.html',
})
export class AddShipaddressPage {

@ViewChild('myInput') myInput: ElementRef;
@ViewChild('myInput') myInputs: ElementRef;

ShippingAddress:any = []
BillingAddress:any = []
billing:any = true;

data:any = {
name:'',
pin:'',
address:'',
landmark:'',
additional:'',
city:'',
state:'',
phoneNumber:'',
type:''	
}

datas:any = {
name:'',
pin:'',
address:'',
landmark:'',
additional:'',
city:'',
state:'',
phoneNumber:'',
type:''  
}


showform:any = false
showBill:any = false
from:any


showDelivery:any = false
showBilling:any = false


showCheckbox:any = true;
Title:any = "Confirm Address"

  constructor(public api :Api,public user: UserProvider,public alert : AlertProvider,public navCtrl: NavController, public navParams: NavParams) {
  this.from = this.navParams.get('from');
  console.log(this.from,"from")

  if(this.from == "delivery"){
  this.showDelivery = true
  this.showBilling = false
  this.Title = "Delivery Address"
  }
  else if(this.from == "billing"){
  this.showDelivery = false
  this.showBilling = true
  this.showCheckbox = false
  this.billing = false
  this.Title = "Billing Address"
  }
  else if(this.from == "cart"){
  this.showDelivery = true
  this.showBilling = true
  }
  }

  ionViewDidLoad() {
  this.getAddress();
  }


  getAddress(){
  	this.alert.Loader.show("Loading..")
  	  	let token = localStorage.getItem("token")
                this.user.UserDetails(token).subscribe((data: any) => {
		            if (data.status == 'ok') {
		             console.log(data,"user detail")
		             for(let i = 0;i<data.data.shipping_address.length;i++){
		             	data.data.shipping_address[i].isChecked = false
                      this.ShippingAddress.push(data.data.shipping_address[i])
		             }

                 for(let i = 0;i<data.data.billing_address.length;i++){
                   data.data.billing_address[i].isChecked = false
                      this.BillingAddress.push(data.data.billing_address[i])
                 }
		                   this.alert.Loader.hide();
		              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  }




  DeleteAddress(val){
  	console.log(val,"delete clicked")
  	 this.user.DeleteShippAddress(val.ship_add_id).subscribe((data: any) => {
		            if (data.status == 'ok') {
		            this.alert.Toast.show("Deleted Successfully")
		                            this.ShippingAddress = []
                this.BillingAddress = []
		            this.getAddress()
		              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  }


   resize() {
      var element = this.myInput['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
      var scrollHeight = element.scrollHeight;
      element.style.height = scrollHeight + 'px';
      this.myInput['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
  } 

  resize1() {
      var element = this.myInputs['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
      var scrollHeight = element.scrollHeight;
      element.style.height = scrollHeight + 'px';
      this.myInputs['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
  }


  addNew(){
  	if(this.showform == false){
  		this.showform = true
  	}
  	else{
  		this.showform = false
  	}
  }



  addAddress(){


    if(this.data.name ==""){
      this.alert.Alert.alert("Please Enter Your Name")
      return
    }

     if(this.data.pin ==""){
      this.alert.Alert.alert("Please Enter Your Pincode")
      return
    }

         if(this.data.type == ""){
      this.alert.Alert.alert("Please Enter Your Address Type")
      return
    }


     if(this.data.address == ""){
      this.alert.Alert.alert("Please Enter Your Address")
      return
    }

     if(this.data.landmark ==""){
      this.alert.Alert.alert("Please Enter Your Landmark")
      return
    }

     if(this.data.city ==""){
      this.alert.Alert.alert("Please Enter Your City")
      return
    }

     if(this.data.state ==""){
      this.alert.Alert.alert("Please Enter Your State")
      return
    } 

    if(this.data.phoneNumber ==""){
      this.alert.Alert.alert("Please Enter Your PhoneNumber")
      return
    }

    if(this.data.phoneNumber.length < 10){
      this.alert.Alert.alert("Invalid PhoneNumber")
      return
    }


  	let token = localStorage.getItem("token")

  	console.log(this.data,"new address")
  	// let data = {
    //             "name" :this.data.name,
    //             "pincode":this.data.pin,
    //             "address":this.data.address,
    //             "landmark":this.data.landmark,
    //             "city":this.data.city,
    //             "state":this.data.state,
    //             "phone":this.data.phoneNumber,
    //             "user_id": token,
    //             "add_type": this.data.type,
    //             "additional_info":this.data.additional
    //     }

    let data = {
      "ship_add_name" :this.data.name,
      "ship_add_pincode":this.data.pin,
      "ship_add_address":this.data.address,
      "ship_add_landmark":this.data.landmark,
      "ship_add_city":this.data.city,
      "ship_add_state":this.data.state,
      "ship_add_phone":this.data.phoneNumber,
      "user_id": token,
      "add_type": this.data.type,
      "ship_add_additional_info":this.data.additional
}

          this.user.AddNewAddress(data).subscribe((data: any) => {
		            if (data.status == 'ok') {
                    this.showform = false

                  this.data = {
                  name:'',
                  pin:'',
                  address:'',
                  landmark:'',
                  additional:'',
                  city:'',
                  state:'',
                  phoneNumber:'',
                  type:''  
                  }

		            this.alert.Toast.show("Address Added Successfully")
		            this.ShippingAddress = []
		            this.getAddress()
		              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })


  }


  cancelSub(){
  	this.data = {
name:'',
pin:'',
address:'',
landmark:'',
additional:'',
city:'',
state:'',
phoneNumber:'',
type:''	
}
  	this.showform = false
  }



  Continue(){
let temp = []
let temp_bill = []

for(let i = 0; i < this.ShippingAddress.length;i++){
if(this.ShippingAddress[i].isChecked == true){
  temp.push(this.ShippingAddress[i])
}
}

for(let i = 0; i < this.BillingAddress.length;i++){

if(this.BillingAddress[i].isChecked == true){
  temp_bill.push(this.BillingAddress[i])
}
}

if(temp.length == 1){
if(temp_bill.length == 0){
if(this.billing == true){
	console.log(this.billing)
	this.navCtrl.push("ShippingPage",{
		shipaddress:temp,
    billaddress:"same"
	})
}
else{
	this.alert.Alert.alert("Please Select The Billing Address")
	return
}
}  else if(temp_bill.length > 1){
               this.alert.Alert.alert("Please Select One Billing Address")
               return
             }

    else if(temp_bill.length == 1){
        this.navCtrl.push("ShippingPage",{
    shipaddress:temp,
    billaddress:temp_bill

  })
    }         

}


 else if(temp.length == 0){
  	         	this.alert.Alert.alert("Please Select the Delivery Address")
  	         	return
  	         }
  	         else if(temp.length > 1){
  	         	this.alert.Alert.alert("Please Select One Shipping Address")
  	         	return
  	         }


  	// this.navCtrl.push("ShippingPage")
  }

  addNewbill(){
    if(this.showBill == false){
      this.showBill = true
    }
    else{
      this.showBill = false
    }
  }


addAddressBill(){
  console.log(this.datas)

      if(this.datas.name ==""){
      this.alert.Alert.alert("Please Enter Your Name")
      return
    }

     if(this.datas.pin ==""){
      this.alert.Alert.alert("Please Enter Your Pincode")
      return
    }

         if(this.datas.type == ""){
      this.alert.Alert.alert("Please Enter Your Address Type")
      return
    }

     if(this.datas.address ==""){
      this.alert.Alert.alert("Please Enter Your Address")
      return
    }


     if(this.datas.landmark ==""){
      this.alert.Alert.alert("Please Enter Your Landmark")
      return
    }

     if(this.datas.city ==""){
      this.alert.Alert.alert("Please Enter Your City")
      return
    }

     if(this.datas.state ==""){
      this.alert.Alert.alert("Please Enter Your State")
      return
    } 

    if(this.datas.phoneNumber ==""){
      this.alert.Alert.alert("Please Enter Your PhoneNumber")
      return
    }

    if(this.datas.phoneNumber.length < 10){
      this.alert.Alert.alert("Invalid PhoneNumber")
      return
    }

      let token = localStorage.getItem("token")

    console.log(this.datas,"new address")
    // let data = {
    //             "name" :this.datas.name,
    //             "pincode":this.datas.pin,
    //             "address":this.datas.address,
    //             "landmark":this.datas.landmark,
    //             "city":this.datas.city,
    //             "state":this.datas.state,
    //             "phone":this.datas.phoneNumber,
    //             "user_id": token,
    //             "add_type": this.datas.type,
    //            "additional_info":this.datas.additional
    //     }
    let data = {
      "bill_add_name" :this.datas.name,
      "bill_add_pincode":this.datas.pin,
      "bill_add_address":this.datas.address,
      "bill_add_landmark":this.datas.landmark,
      "bill_add_city":this.datas.city,
      "bill_add_state":this.datas.state,
      "bill_add_phone":this.datas.phoneNumber,
      "user_id": token,
      "add_type": this.datas.type,
      "bill_add_additional_info":this.datas.additional
}
          this.user.AddBillAddress(data).subscribe((data: any) => {
                if (data.status == 'ok') {
                    this.showBill = false
                    this.datas = {
                      name:'',
                      pin:'',
                      address:'',
                      landmark:'',
                      additional:'',
                      city:'',
                      state:'',
                      phoneNumber:'',
                      type:''  
                      }

                this.alert.Toast.show("Address Added Successfully")
                this.ShippingAddress = []
                this.getAddress()
                  }
            else {
                this.showBill = false
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.showBill = false
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })


}

cancelSubs(){
  this.datas = {
name:'',
pin:'',
address:'',
landmark:'',
additional:'',
city:'',
state:'',
phoneNumber:'',
type:''  
}
 this.showBill = false
}


  DeleteBillAddress(val){
    console.log(val,"delete clicked")
     this.user.DeletebilingAddress(val.bill_add_id).subscribe((data: any) => {
                if (data.status == 'ok') {
                this.alert.Toast.show("Deleted Successfully")
                this.ShippingAddress = []
                this.BillingAddress = []
                this.getAddress()
                  }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  }


}
