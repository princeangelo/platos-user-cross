import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddShipaddressPage } from './add-shipaddress';

@NgModule({
  declarations: [
    AddShipaddressPage,
  ],
  imports: [
    IonicPageModule.forChild(AddShipaddressPage),
  ],
})
export class AddShipaddressPageModule {}
