import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ActionSheetController, LoadingController, Platform, ToastController } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { Api } from '../../providers/api/api';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AlertProvider } from '../../providers/alert/alert';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the CertificateEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-certificate-edit',
  templateUrl: 'certificate-edit.html',
})
export class CertificateEditPage {

  data: any = {
    GST_image: '',
    Food_image: '',
    picName: ''
}
imageURIs = ""
imageURI
getData: any
Food_certi
GST_path
PAN_path
imageURI1
imageURI_1
imageURIss
adduser
is_nonveg: boolean
showDiv: any = false
catt_upload_file: any = []
Certificate: any = []
certi_count: any
certifi_delete: any = false
anArrays: any = []
Holidays: any = []
public alertPresented: any;
constructor(public events: Events, public photoViewer: PhotoViewer, public actionSheetCtrl: ActionSheetController, public zone: NgZone, public navCtrl: NavController, public navParams: NavParams, private transfer: FileTransfer, private camera: Camera, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public user: UserProvider, public alert: AlertProvider, public api: Api, public platform: Platform) {
    this.alertPresented = false
    this.Food_certi = this.api.Food_certificate
    this.GST_path = this.api.GST_certificate
    this.PAN_path = this.api.PAN_certificate
    this.getData = this.navParams.get("data")
    console.log(this.getData, "wholedata")
    this.imageURI = this.getData.usr_gst_certificate;
    this.imageURIs = this.getData.usr_pan_certificate;
    this.Certificate = this.getData.ceritificates
    console.log(this.Certificate, "certi")
    this.certi_count = this.Certificate.length
}
ionViewDidLoad() {
    console.log('ionViewDidLoad CertificateEditPage');
}
fullView(value) {
    console.log(value)
    this.photoViewer.show(this.GST_path + value);
}
fullViews(value) {
    console.log(value)
    this.photoViewer.show(this.PAN_path + value);
}
fullViews1(value) {
  console.log(value)
  this.photoViewer.show(this.Food_certi + value);
}
gstPath(val) {
    let data = this.GST_path + val;
    return data
}
panPath(val) {
  let data = this.PAN_path + val;
  return data
}
otherCertificatePath(val) {
    let data = this.Food_certi + val;
    return data
}
backButtonAction() {
    this.navCtrl.pop()
}
randomFileName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    var n = new Date()
    var date = n.toISOString();
    return text + "-" + date + ".png";
}
takeSnap() {
    const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    }
    this.camera.getPicture(options).then((imageData) => {
        this.imageURI1 = imageData;
        this.uploadFileGST();
        // this.imagebox = this.imageURI;
        // console.log(this.imagebox,"uri")
    }, (err) => {
        console.log(err);
        this.presentToast(err);
    });
}

takeSnap1() {
  const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  }
  this.camera.getPicture(options).then((imageData) => {
      this.imageURI_1 = imageData;
      this.uploadFilePAN();
      // this.imagebox = this.imageURI;
      // console.log(this.imagebox,"uri")
  }, (err) => {
      console.log(err);
      this.presentToast(err);
  });
}






uploadFileGST() {
    return new Promise((resolve, reject) => {
        let loader = this.loadingCtrl.create({
            content: "Uploading..."
        });
        loader.present();
        const fileTransfer: FileTransferObject = this.transfer.create();
        let options: FileUploadOptions = {
            fileKey: 'gstpic',
            fileName: this.randomFileName(),
            chunkedMode: false,
            mimeType: "image/png",
            headers: {}
        }
        let data = [];
        data.push(new Promise((resolve, reject) => {
            resolve(fileTransfer.upload(this.imageURI1, 'http://13.234.1.45/api/usergst_image', options).then((data) => {
                console.log(data, " Uploaded Successfully");
                let temp = JSON.parse(data.response)
                let imgURL = temp.data;
                this.data.GST_image = imgURL.gstpic;
                this.imageURI = imgURL.gstpic;
                console.log(this.imageURI, "gst")
                // this.data.menu_images.push(imgURL)
                loader.dismiss();
                // this.presentToast("Image uploaded successfully");
            }, (err) => {
                console.log(err);
                loader.dismiss();
                // this.presentToast(err);
            }))
        }))
        Promise.all(data).then((values) => {
            resolve(values)
            this.updateCertificate()
        });
    })
}


uploadFilePAN() {
  return new Promise((resolve, reject) => {
      let loader = this.loadingCtrl.create({
          content: "Uploading..."
      });
      loader.present();
      const fileTransfer: FileTransferObject = this.transfer.create();
      let options: FileUploadOptions = {
          fileKey: 'panpic',
          fileName: this.randomFileName(),
          chunkedMode: false,
          mimeType: "image/png",
          headers: {}
      }
      let data = [];
      data.push(new Promise((resolve, reject) => {
          resolve(fileTransfer.upload(this.imageURI_1, 'http://13.234.1.45/api/userpan_image/', options).then((data) => {
              console.log(data, " Uploaded Successfully");
              let temp = JSON.parse(data.response)
              let imgURL = temp.data;
              this.data.GST_image = imgURL.gstpic;
              this.imageURI = imgURL.gstpic;
              console.log(this.imageURI, "gst")
              // this.data.menu_images.push(imgURL)
              loader.dismiss();
              // this.presentToast("Image uploaded successfully");
          }, (err) => {
              console.log(err);
              loader.dismiss();
              // this.presentToast(err);
          }))
      }))
      Promise.all(data).then((values) => {
          resolve(values)
         this.updateCertificate()
      });
  })
}













presentToast(msg) {
    let toast = this.toastCtrl.create({
        message: msg,
        duration: 3000,
        position: 'bottom'
    });
    toast.onDidDismiss(() => {
        console.log('Dismissed toast');
    });
    toast.present();
}
deleteImage(index) {
    console.log(index)
    // if (index > -1) {
    // this.imagebox.splice(index, 1);
    this.imageURI = ''
    // }
}
deleteImages(index) {
    console.log(index)
    // if (index > -1) {
    // this.imageboxFood.splice(index, 1);
    this.imageURIs = ''
    // }
}
updateCertificate() {
    this.alert.Loader.show("Uploading..")
    console.log("in upload")
    let item = localStorage.getItem("token")
    if (this.getData.add_user != null) {
        this.adduser = [{
          add_user_id: this.getData.add_user.add_user_id,
          add_user_name: this.getData.add_user.add_user_name,
          add_user_mobile:this.getData.add_user.add_user_mobile,
          add_user_email: this.getData.add_user.add_user_email,
          user_id: this.getData.add_user.user_id
        }]
    } else {
        this.adduser = []
    }
 
    let datas = {
       
    }
    console.log("final data sent", datas)
    this.user.toSignUp_IMG(this.getData.user_id,this.data).subscribe(async (data: any) => {
        console.log(data, "response")
        console.log("api hit")
        let token: any = localStorage.getItem("token")
        let datas: any = {
            token: '',
        }
        datas.token = token;
        this.user.UserDetails(datas).subscribe((data: any) => {
             this.events.publish('audit:status', data.data.audit_deactive);
            this.getData = data.data;
            this.imageURI = this.getData.usr_gst_certificate;
            this.imageURIs = this.getData.usr_pan_certificate;
            this.Certificate = this.getData.ceritificates
            console.log(this.Certificate, "certi")
            this.certi_count = this.Certificate.length
            this.data.picName = ''
            this.imageURIs = ''
            // this.catt_upload_file = []
            console.log(this.getData, "see")
            this.presentToast("Image uploaded successfully");
            this.showDiv = false
            this.alert.Loader.hide();
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        //   if(data.data.status == 'ok'){
        // await this.apihit()
        //   }
    }, (error) => {
        this.alert.Loader.hide();
        this.alert.Alert.alert("Cannot Connect To Server")
        return;
    })
}
apihit() {
    console.log("api hit")
    let token: any = localStorage.getItem("token")
    let data: any = {
        token: '',
    }
    data.token = token;
    this.user.UserDetails(data).subscribe((data: any) => {
         this.events.publish('audit:status', data.data.audit_deactive);
        this.getData = data.data;
        console.log(this.getData, "see")
        this.presentToast("Image uploaded successfully");
        this.alert.Loader.hide();
    }, (error) => {
        this.alert.Loader.hide();
        this.alert.Alert.alert("Cannot Connect To Server")
        return;
    })
}
onChangeName() {
    console.log("typesd something")
}
getImage() {
    if (this.data.picName) {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
        }
        this.camera.getPicture(options).then((imageData) => {
            this.imageURIss = imageData;
            this.uploadFileFood()
        }, (err) => {
            console.log(err);
            this.presentToast(err);
        });
    } else {
        this.alert.Alert.alert("Please Enter The Certificate Name")
        return
    }
}
uploadFileFood() {
    return new Promise((resolve, reject) => {
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        const fileTransfer: FileTransferObject = this.transfer.create();
        let options: FileUploadOptions = {
            fileKey: 'foodpic[]',
            fileName: this.randomFileName(),
            chunkedMode: false,
            mimeType: "image/png",
            headers: {}
        }
        let data = [];
        data.push(new Promise((resolve, reject) => {
            resolve(fileTransfer.upload(this.imageURIss, 'http://platos.in/api/catfood_image', options).then((data) => {
                console.log(data, " Uploaded Successfully");
                let temp = JSON.parse(data.response)
                let imgURL = temp.data[0];
                this.imageURIs = imgURL;
                debugger
                // }
                // else{
                //   this.catt_upload_file = []
                //    this.catt_upload_file.push({"certify_name" : this.data.picName, "certify_file":this.imageURIs, "certify_id":''})
                // }
                // this.data.menu_images.push(imgURL)
                loader.dismiss();
            }, (err) => {
                console.log(err);
                loader.dismiss();
                // this.presentToast(err);
            }))
        }))
        Promise.all(data).then((values) => {
            resolve(values)
        });
    })
}
async uploadPic() {
    let data = {
        name: this.data.picName,
        img: this.imageURIs
    }
    debugger
    if (data.name == '') {
        this.alert.Alert.alert("Certificate Name Can't Be Empty")
        return
    }
    if (!data.img) {
        this.alert.Alert.alert("Please Upload the Certificate")
        return
    }
    let temp_info = {
        "certify_name": this.data.picName,
        "certify_file": this.imageURIs,
        "certify_id": ''
    }
    this.catt_upload_file = temp_info;
    console.log(data, "check file")
    //this.updateCertificate()
}
// pageRefresh(){
//  this.alert.Loader.show("Loading")    
//       let token:any = localStorage.getItem("tokens")
// let data:any = {
//   token:'',
// }
// data.token = token;
//       this.user.getCaterer(data).subscribe((data: any) => {
//       this.getData = data.data;
//          this.imageURI = this.getData.catt_gst_certificate;
//    this.imageURIs = this.getData.catt_food_certificate;
//    this.Certificate = this.getData.ceritificates
//    console.log(this.Certificate,"certi")
//    this.certi_count = this.Certificate.length
//       this.alert.Loader.hide();
//         }, (error) => {
//             this.alert.Loader.hide();
//             this.alert.Alert.alert("Cannot Connect To Server")
//             return;
//         })
//  }
show() {
    if (this.certifi_delete == false) {
        this.certifi_delete = true
    } else {
        this.certifi_delete = false
    }
}
async CertificateDelete(certi: any) {
    console.log(certi)
    this.alert.Loader.show("Loading")
    this.user.deleteOtherCertificates(certi.certify_id).subscribe((data: any) => {
        if (data.status == "ok") {
            let token: any = localStorage.getItem("token")
            let data: any = {
                token: '',
            }
            data.token = token;
            this.user.UserDetails(data).subscribe((data: any) => {
                 //this.events.publish('audit:status', data.data.audit_deactive);
                this.getData = data.data;
                this.imageURI = this.getData.usr_gst_certificate;
                this.imageURIs = this.getData.usr_pan_certificate;
                this.Certificate = this.getData.ceritificates
                console.log(this.Certificate, "certi")
                this.certi_count = this.Certificate.length
                this.alert.Toast.show("Certificate Deleted")
                this.certifi_delete = false
                this.alert.Loader.hide();
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        } else {
            this.alert.Loader.hide()
            this.alert.Alert.alert(data.data)
            this.certifi_delete = false
            return
        }
    }, (error) => {
        this.alert.Loader.hide();
        this.alert.Alert.alert("Cannot Connect To Server")
        this.certifi_delete = false
        return;
    })
}
public PANCertificateDelete() {
  this.alertPresented = true
  localStorage.setItem("alert", this.alertPresented);
  let actionSheet = this.actionSheetCtrl.create({
      title: 'GST Certificate',
      buttons: [{
          text: 'Delete',
          icon: 'trash',
          handler: () => {
              this.alertPresented = false
              localStorage.setItem("alert", this.alertPresented);
              this.deletePan()
          }
      }, {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
              this.alertPresented = false
              localStorage.setItem("alert", this.alertPresented);
              this.certifi_delete = false
          }
      }],
  });
  actionSheet.present();
}


public GSTCertificateDelete() {
    this.alertPresented = true
    localStorage.setItem("alert", this.alertPresented);
    let actionSheet = this.actionSheetCtrl.create({
        title: 'GST Certificate',
        buttons: [{
            text: 'Delete',
            icon: 'trash',
            handler: () => {
                this.alertPresented = false
                localStorage.setItem("alert", this.alertPresented);
                this.deleteGst()
            }
        }, {
            text: 'Cancel',
            icon: 'close',
            role: 'cancel',
            handler: () => {
                this.alertPresented = false
                localStorage.setItem("alert", this.alertPresented);
                this.certifi_delete = false
            }
        }],
    });
    actionSheet.present();
}
public OtherCertificateDelete(name: any) {
    this.alertPresented = true
    localStorage.setItem("alert", this.alertPresented);
    let actionSheet = this.actionSheetCtrl.create({
        title: name.certify_file,
        buttons: [{
            text: 'Delete',
            icon: 'trash',
            handler: () => {
                this.alertPresented = false
                localStorage.setItem("alert", this.alertPresented);
                this.CertificateDelete(name)
            }
        }, {
            text: 'Cancel',
            icon: 'close',
            role: 'cancel',
            handler: () => {
                this.alertPresented = false
                localStorage.setItem("alert", this.alertPresented);
                this.certifi_delete = false
            }
        }],
    });
    actionSheet.present();
}
deleteGst() {
    let token = localStorage.getItem("token")
    let data: any = {
        ceritificate: "certificate",
        token: token
    }
    console.log(data)
    // this.alert.presentDelete().then((res) => { 
    this.user.deleteCertificate(data).subscribe((data: any) => {
        if (data.status == "ok") {
            this.alert.Loader.hide();
            // this.alert.Toast.show("Deleted Successfully")
            this.certifi_delete = false
            let token: any = localStorage.getItem("token")
            let data: any = {
                token: '',
            }
            data.token = token;
            this.user.UserDetails(data).subscribe((data: any) => {
               //  this.events.publish('audit:status', data.data.audit_deactive);
                this.getData = data.data;
                this.imageURI = this.getData.usr_gst_certificate;
                this.imageURIs = this.getData.usr_pan_certificate;
                this.Certificate = this.getData.ceritificates
                console.log(this.Certificate, "certi")
                this.certi_count = this.Certificate.length
                this.alert.Toast.show("Certificate Deleted")
                this.alert.Loader.hide();
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                this.certifi_delete = false
                return;
            })
        } else {
            this.alert.Loader.hide()
            this.alert.Alert.alert(data.data)
            this.certifi_delete = false
            return
        }
    }, (error) => {
        this.alert.Loader.hide();
        this.alert.Alert.alert("Cannot Connect To Server")
        this.certifi_delete = false
        return;
    })
    //   }, err => {
    // this.alert.Loader.hide();
    //      console.log(err)
    this.certifi_delete = false
    // }) 
}


deletePan() {
  let token = localStorage.getItem("token")
  let data: any = {
      ceritificate: "certificate",
      token: token
  }
  console.log(data)
  // this.alert.presentDelete().then((res) => { 
  this.user.deleteCertificate1(data).subscribe((data: any) => {
      if (data.status == "ok") {
          this.alert.Loader.hide();
          // this.alert.Toast.show("Deleted Successfully")
          this.certifi_delete = false
          let token: any = localStorage.getItem("token")
          let data: any = {
              token: '',
          }
          data.token = token;
          this.user.UserDetails(data).subscribe((data: any) => {
             //  this.events.publish('audit:status', data.data.audit_deactive);
              this.getData = data.data;
              this.imageURI = this.getData.usr_gst_certificate;
              this.imageURIs = this.getData.usr_pan_certificate;
              this.Certificate = this.getData.ceritificates
              console.log(this.Certificate, "certi")
              this.certi_count = this.Certificate.length
              this.alert.Toast.show("Certificate Deleted")
              this.alert.Loader.hide();
          }, (error) => {
              this.alert.Loader.hide();
              this.alert.Alert.alert("Cannot Connect To Server")
              this.certifi_delete = false
              return;
          })
      } else {
          this.alert.Loader.hide()
          this.alert.Alert.alert(data.data)
          this.certifi_delete = false
          return
      }
  }, (error) => {
      this.alert.Loader.hide();
      this.alert.Alert.alert("Cannot Connect To Server")
      this.certifi_delete = false
      return;
  })
  //   }, err => {
  // this.alert.Loader.hide();
  //      console.log(err)
  this.certifi_delete = false
  // }) 
}


}