import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CertificateEditPage } from './certificate-edit';

@NgModule({
  declarations: [
    CertificateEditPage,
  ],
  imports: [
    IonicPageModule.forChild(CertificateEditPage),
  ],
})
export class CertificateEditPageModule {}
