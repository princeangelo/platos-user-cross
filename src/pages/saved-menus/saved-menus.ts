import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { NgZone } from '@angular/core';
import { Events } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-saved-menus',
  templateUrl: 'saved-menus.html',
})
export class SavedMenusPage {

Cart_Count:any = 0;

  constructor(public events: Events,public _zone: NgZone,public api :Api,public user: UserProvider,public alert : AlertProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  	this.getCount()
    console.log('ionViewDidLoad SavedMenusPage');
  }

getCount(){
 this._zone.run(() =>   {
  let token = localStorage.getItem("token")
         this.user.Notification(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               // this.alert.Toast.show("Added to Cart")
               console.log(data,"notification data")
               this.Cart_Count = data.data.cart_count
               let count:boolean = this.Cart_Count
               this.events.publish('cart:refresh', count);
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    })
 
}
  

}
