import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavedMenusPage } from './saved-menus';

@NgModule({
  declarations: [
    SavedMenusPage,
  ],
  imports: [
    IonicPageModule.forChild(SavedMenusPage),
  ],
})
export class SavedMenusPageModule {}
