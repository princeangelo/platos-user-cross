import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';

/**
 * Generated class for the ContractCateringPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contract-catering',
  templateUrl: 'contract-catering.html',
})
export class ContractCateringPage {


  data:any = {
    name:'',
    pincode:'',
    type:'',
    date:'',
    month:'',
    budget:'',
    person:'',
    addres:'',
    meal_typ:'',
    cusi_typ:'',
    vegie:'',
    con_name:'',
    phon_num:'',
    veg:''
  }
  user_id:any;
  mealTypes:any;
  cusineTypes:any;
  checked:any=[];
  checked1:any=[];
   constructor(public events: Events,public _zone: NgZone,public navCtrl: NavController, public navParams: NavParams,public api :Api,public user: UserProvider,public alert : AlertProvider) {
    this.user_id = this.navParams.get('user_id')
  }
 
   ionViewDidLoad() {
     this.getMealList();
     this.getCusinList();
     console.log('ionViewDidLoad EventCateringPage');
   }
 
   getMealList(){
 
     this._zone.run(() =>  {
     //  this.alert.Loader.show("Loading")
     
            this.user.getMealTypes().subscribe((data: any) => {
                if (data.status == 'ok') {
                  this.mealTypes = data.data;
                  console.log(JSON.stringify(data.data));
 
                  for(let j = 0;j<this.mealTypes.length;j++){
                
                    this.mealTypes[j].selected = false
                  }
                       //this.alert.Loader.hide();
                  }
                else {
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
     
     })
   }
   
  selectMember(event, checkbox : String) { 
     if (event.checked ) {
       this.checked.push(checkbox);
       this.alert.Alert.alert(this.checked)
     } else {
       let index = this.removeCheckedFromArray(checkbox);
       this.checked.splice(index,1);
     }
   }
   selectMember1(event, checkbox : String) { 
     if (event.checked ) {
       this.checked1.push(checkbox);
       this.alert.Alert.alert(this.checked1)
     } else {
       let index = this.removeCheckedFromArray1(checkbox);
       this.checked1.splice(index,1);
     }
   }
   //Removes checkbox from array when you uncheck it
   removeCheckedFromArray1(checkbox : String) {
     return this.checked1.findIndex((category)=>{
       return category === checkbox;
     })
   }
 
   removeCheckedFromArray(checkbox : String) {
     return this.checked.findIndex((category)=>{
       return category === checkbox;
     })
   }
 
 
 
 
   //Empties array with checkedboxes
   emptyCheckedArray() {
     this.checked = [];
     this.checked1 = [];
   }
 
  getCheckedBoxes() {
    //Do whatever
    console.log(this.checked);
  }
   
   getCusinList(){
 
     this._zone.run(() =>  {
      // this.alert.Loader.show("Loading")
     
            this.user.getCusineTypes().subscribe((data: any) => {
                if (data.status == 'ok') {
                  this.cusineTypes = data.data;
                  console.log(JSON.stringify(data.data));
 
                  for(let j = 0;j<this.cusineTypes.length;j++){
                
                    this.cusineTypes[j].selected = false
                  }
                      // this.alert.Loader.hide();
                  }
                else {
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
     
     })
   }
 
   logForm(){
    
    // this.menu.enable(true)
    
    // this.navCtrl.setRoot('DashboardPage')
    if(this.data.name ==""){
      this.alert.Alert.alert("Please Enter Company Name")
      return
    }
    if(this.data.addres == ""){
      this.alert.Alert.alert("Please Enter Your Address")
      return
    }
     if(this.data.pin ==""){
      this.alert.Alert.alert("Please Enter Your Pincode")
      return
    }

     if(this.data.budget == ""){
      this.alert.Alert.alert("Please Enter Your budget")
      return
    }

     if(this.data.guest ==""){
      this.alert.Alert.alert("Please Enter Your guest")
      return
    }

     if(this.data.con_name ==""){
      this.alert.Alert.alert("Please Enter Contact name")
      return
    }

     if(this.data.phon_num ==""){
      this.alert.Alert.alert("Please Enter contact number")
      return
    } 

   
    if(this.data.phon_num.length < 10){
      this.alert.Alert.alert("Invalid PhoneNumber")
      return
    }
   

  
           // document.write(array.toString()); 
            let data1 = {
              "user_id": this.user_id,
              "company_name": this.data.name,
              "address": this.data.addres,
              "post_code": this.data.pincode,
              "start_date": this.data.date,   
              "contract_length": this.data.month, 
              "budget": this.data.budget,
              "guests": this.data.guest,
              "is_veg":"true",  
              "meal_type":   this.checked.join(),    
              "cuisine_type":  this.checked1.join(),
              "add_info": this.data.veg,
              "others_mt": this.data.meal_typ,
              "others_ct": this.data.cusi_typ,   
              "name": this.data.con_name,
              "ph_number": this.data.phon_num                                                                   
        } 
    
    
        this.alert.Loader.show("Loading..")
        console.log(data1)
    
            this.user.contract_submit(data1).subscribe((data: any) => {
          console.log(data)
                if (data.status == 'ok') {
                    this.alert.Loader.hide();
                    this.alert.Toast.show("Contract Catering Added Successfully");
                    this.navCtrl.setRoot('DashboardPage')
                          
                          // this.createUser(data.data.catt_first_name)
                          // this.navCtrl.setRoot(MainPage)
                       // }
                  }
                else {
                    this.alert.Loader.hide();
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
    
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
      }

 
    moveFocus(nextElement) {
         nextElement.setFocus();
     }
 
 }
 