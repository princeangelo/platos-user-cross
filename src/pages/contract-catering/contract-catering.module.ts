import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContractCateringPage } from './contract-catering';

@NgModule({
  declarations: [
    ContractCateringPage,
  ],
  imports: [
    IonicPageModule.forChild(ContractCateringPage),
  ],
})
export class ContractCateringPageModule {}
