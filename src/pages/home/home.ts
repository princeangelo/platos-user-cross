import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

forarray: any=[
{src: 'assets/imgs/home/computer.png', name: 'ORDER ONLINE', para: 'Lorem Ipsum is simply dummy text of the printing and typesetting.'},
{src: 'assets/imgs/home/calendar.png', name: 'Event Catering', para: 'Lorem Ipsum is simply dummy text of the printing and typesetting.'},
{src: 'assets/imgs/home/computer.png', name: 'Contract Catering', para: 'Lorem Ipsum is simply dummy text of the printing and typesetting.'},
{src: 'assets/imgs/home/computer.png', name: 'ORDER ONLINE', para: 'Lorem Ipsum is simply dummy text of the printing and typesetting.'}

];

  constructor(public navCtrl: NavController) {

  }

  buy(){
  this.navCtrl.push("MyCartPage")
}

}
