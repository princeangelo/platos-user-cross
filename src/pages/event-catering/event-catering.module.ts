import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventCateringPage } from './event-catering';

@NgModule({
  declarations: [
    EventCateringPage,
  ],
  imports: [
    IonicPageModule.forChild(EventCateringPage),
  ],
})
export class EventCateringPageModule {}
