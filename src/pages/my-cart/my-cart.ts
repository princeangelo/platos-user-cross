import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { NgZone } from '@angular/core';
/**
 * Generated class for the MyCartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-cart',
  templateUrl: 'my-cart.html',
})
export class MyCartPage {

cart_Items:any
minVal:any 
maxVal:any
is_recommended:any

total:any = 0;

  constructor(public _zone: NgZone,public api :Api,public user: UserProvider,public alert : AlertProvider,public navCtrl: NavController, public navParams: NavParams) {
  // this.minVal = this.navParams.get("min")
  // this.maxVal = this.navParams.get("max")
  }

  ionViewDidLoad() {
  	let token = localStorage.getItem("token")
     this.user.ViewCartItems(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               console.log(data,"cart items")
               this.cart_Items = data.data;
               this.minVal = this.cart_Items[0].caterer.catt_min_order
               this.maxVal = this.cart_Items[0].caterer.catt_max_order
               for(let i = 0;i < this.cart_Items.length ; i++){
                 this.total = this.total + (this.cart_Items[i].quantity * this.cart_Items[i].price)
               }
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  }


  deleteItem(value:any){
  	console.log(value,"from delete")
 this._zone.run(() =>   {
  	 this.user.DeleteCartItem(value.cart_id).subscribe((data: any) => {
            if (data.status == 'ok') {
              this.alert.Toast.show("Removed Successfully")
                	let token = localStorage.getItem("token")
     this.user.ViewCartItems(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               console.log(data,"cart items")
               this.cart_Items = data.data;
               this.total = 0
                for(let i = 0;i < this.cart_Items.length ; i++){
                 debugger
                 this.total = this.total + (this.cart_Items[i].quantity * this.cart_Items[i].price)
               }
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
})
  }



    minus(index,num,id){

  if(this.cart_Items[index].quantity == this.minVal){
  console.log("reached min value")
  }else{
  	this.cart_Items[index].quantity = this.cart_Items[index].quantity - 1;
  	let data:any = {
  		quantity: '',
  		item_id: ''
  	}
  	data.quantity = this.cart_Items[index].quantity
  	data.item_id = id
  	console.log(data,"before api hit")
 this._zone.run(() =>   {  	
  	this.user.UpdateCartItem(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Cart Updated")
               let token = localStorage.getItem("token")
                this.user.ViewCartItems(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               console.log(data,"cart items")
               this.cart_Items = data.data;
               this.total = 0
                for(let i = 0;i < this.cart_Items.length ; i++){
                 debugger
                 this.total = this.total + (this.cart_Items[i].quantity * this.cart_Items[i].price)
               }
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  })
  }
  }

 plus(index,num,id){
 	if(this.cart_Items[index].quantity == this.maxVal){
     console.log("reached max value")
 	}
 	else{
    this.cart_Items[index].quantity = this.cart_Items[index].quantity + 1;
    	let data:any = {
  		quantity: '',
  		item_id: ''
  	}
  	data.quantity = this.cart_Items[index].quantity
  	data.item_id = id
  	console.log(data,"before api hit")
  this._zone.run(() =>   { 	
  	this.user.UpdateCartItem(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Cart Updated")
               let token = localStorage.getItem("token")
           this.user.ViewCartItems(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               console.log(data,"cart items")
               this.cart_Items = data.data;
               this.total = 0
               for(let i = 0;i < this.cart_Items.length ; i++){
               	debugger
                 this.total = this.total + (this.cart_Items[i].quantity * this.cart_Items[i].price)
               }
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  })
 	}
 }

numberWithCommas(n) {
   var parts=n.toString().split(".");
   return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

toMenu(){
  this.navCtrl.setRoot("OrderPage")
}

updateCart(){
  this.navCtrl.push("AddShipaddressPage",{
    from:"cart"
  })
}

}
