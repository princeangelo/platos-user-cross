import { Component,ViewChild } from '@angular/core';
import { Api } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { NgZone } from '@angular/core';
import { Events } from 'ionic-angular';
import { IonicPage, NavController, NavParams , Slides , Content,Platform} from 'ionic-angular';

/**
 * Generated class for the CatererDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-caterer-detail',
  templateUrl: 'caterer-detail.html',
})
export class CatererDetailPage {
SlideArr:any = [{"menu_imgname":"assets/imgs/cat_video_img.png"}]
// number:any = 0;
dis_but:any = false
caterdata:any
totalPages
hasMoreData:any = false
page_count:any = 1
defaultMenuPic = "assets/imgs/no_image.jpg"

MenuList:any =[]
minVal:number
maxVal:number

Cart_Count:any = 0;
Slide_Arry:any = []
ImagePath:any
infiniteScrollEnabled = true
Cater_Img:any 
recommendedMenu:any = []
menuGroups:any = []
cartItem:any

AllMenu:any = []
 @ViewChild('myContent') content: Content;
 @ViewChild(Slides) slides: Slides;
 @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides ;
  @ViewChild('MultiItemsScrollingTabs') ItemsTitles : Content;

  SwipedTabsIndicator :any= null;
  tabTitleWidthArray :any= [];
  tabElementWidth_px :number= 50;
  screenWidth_px :number= 0;
  isRight :boolean= true;
  isLeft:boolean= true;
  tabs:any=[];
  
  indexValue:any = 0
  isRecommended :any = false
  showImage:any =false
  groupedMenus:any = []
  divPositionsArr:any = []
  mi = "minus"
  pl = "plus"
  constructor(platform: Platform,public events: Events,public _zone: NgZone,public api :Api,public user: UserProvider,public alert : AlertProvider,public navCtrl: NavController, public navParams: NavParams) {
  this.caterdata = this.navParams.get("data")
  this.ImagePath = this.api.Menu_image;
  this.Cater_Img = this.api.filepath;
  console.log(this.caterdata,"got")

 this.tabs=[];
 console.log(this.tabs)
    console.log('Width: ' + platform.width());
    this.screenWidth_px=platform.width();

  debugger
  this.minVal = parseInt(this.caterdata.catt_min_order)
  this.maxVal = parseInt(this.caterdata.catt_max_order)
  // this.number = this.minVal

  this.getAllCarterMenu()
  }


  scrolling(event){
    // console.log(event)    

  }

  scrollComplete(event){
    this.divPositions(event)
      // console.log(event)

  }


  divPositions(event){
      this.divPositionsArr = []

    for (var i = 0; i < this.menuGroups.length; ++i) {
        let name = "menu"+i;
        let y = document.getElementById("menu"+i).offsetTop;
        // y = y + 85;
        this.divPositionsArr.push({"name": name,"position":y})
    }


    for (var j = 0; j < this.divPositionsArr.length; ++j) {
      if(this.divPositionsArr[j].position <= event.scrollTop){
        if(j+1 != this.divPositionsArr.length){
        if(event.scrollTop <= this.divPositionsArr[j+1].position){
        console.log(this.divPositionsArr[j].name,"is Active")
        this.selectTab(j)
        return
       }
      }
      else{
         console.log(this.divPositionsArr[this.divPositionsArr.length-1].name,"is Active")
         this.selectTab(j)
         return
      }
     }
     else{
        console.log(this.divPositionsArr[j].name,"is Active")
         this.selectTab(j)
         return
     }
    }
   
  }


 ionViewDidEnter() {
        this._zone.run(() => {

    this.SwipedTabsIndicator = document.getElementById("indicator");
    for (let i in this.tabs)
      this.tabTitleWidthArray.push(document.getElementById("tabTitle"+i).offsetWidth);

     this.selectTab(0);
   })

  this.checkCart()
  }

  ionViewWillEnter(){
     this.getCount()
    this.content.scrollToTop();
  }







  menuDetail(value){
    this.navCtrl.push("MenuDetailPage",{
      data: value,
      caterData: this.caterdata
    })
    console.log(value)
  }

  scrollTo(id) {
        console.log("menu"+id)
        let y = document.getElementById("menu"+id).offsetTop;
        
        y = y + 95;
        console.log(y,"Position")
        this.content.scrollTo(0, y, 1000)
    }

  groupedMenuName(i){
     let data = this.menuGroups[i]
      return data
  }  




  scrollIndicatiorTab()
  {
    console.log("line 88")
    this.ItemsTitles.scrollTo(this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())-this.screenWidth_px/2,0);
  }

  selectTab(index)
  {
    this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[index]+"px";
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';
    this.SwipedTabsSlider.slideTo(index);
    this.indexValue = index
    if(this.isRecommended == true){
       if(index == 0){
         this.showImage = true
       }
       else{
         this.showImage = false
       }
    }


  }


  calculateDistanceToSpnd(index)
  {
    var result=0;
    for (var _i = 0; _i < index; _i++) {
      result=result+this.tabTitleWidthArray[_i];
    }
    return result;
  }

  updateIndicatorPosition() {
    console.log("line 110")
    var index=this.SwipedTabsSlider.getActiveIndex();
    if( this.SwipedTabsSlider.length()==index)
      index=index-1;

    this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[index]+"px";
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';

  }

  updateIndicatorPositionOnTouchEnd()
  {
    console.log("line 122")
    setTimeout( () => { this.updateIndicatorPosition(); }, 200);
  }


    animateIndicator($event)
  {
    console.log("line 127")
    this.isLeft=false;
    this.isRight=false;
    var currentSliderCenterProgress =(1/(this.SwipedTabsSlider.length()-1) )*this.SwipedTabsSlider.getActiveIndex();
    if($event.progress < currentSliderCenterProgress)
    {
      this.isLeft=true;
      this.isRight=false;

    } if($event.progress > currentSliderCenterProgress)
    {
      this.isLeft=false;
      this.isRight=true;
    }

    if(this.SwipedTabsSlider.isEnd())
      this.isRight=false;

    if( this.SwipedTabsSlider.isBeginning())
      this.isLeft=false;

    if(this.isRight)
      this.SwipedTabsIndicator.style.webkitTransform =
      'translate3d('+( this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())
        +($event.progress - currentSliderCenterProgress) *(this.SwipedTabsSlider.length()-1)*this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()+1])
      +'px,0,0)';

    

    if(this.isLeft)
      this.SwipedTabsIndicator.style.webkitTransform =
      'translate3d('+( this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())
        +($event.progress - currentSliderCenterProgress) *(this.SwipedTabsSlider.length()-1)*this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()-1])
      +'px,0,0)';

    

    if(!this.isRight && !this.isLeft)
      this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()]+"px";

  }





  ionViewDidLoad() {
   
    console.log('ionViewDidLoad CatererDetailPage');
  }


 getImage(value){
 let data = this.Cater_Img+value;
 return data
 }


 onChangeNo(val:any,index){
  if(val >= this.minVal){
    if(val <= this.maxVal){
     this.MenuList[index].number = val;
    }
    else{
      this.MenuList[index].number = this.maxVal;
    }
  }
  else{
     this.MenuList[index].number = this.minVal;
  }
 }

 slideImage(){

     let temp = this.caterdata.menus
  let count = 0;

if(temp){
  if(temp.length > 0){
      for(let i = 0 ; i < temp.length ; i++){
        if(temp[i].images.length > 0){
             for(let j = 0;j < temp[i].images.length;j++){
            count = count + 1;
            this.Slide_Arry.push(temp[i].images[j])
            if(count >= 6){
            return
           }
          }
        }
     }
  }
}

  
  console.log(this.Slide_Arry,"image array")

 }

 getAllGroupList(id){
       this.user.getAllGroupMenu(id).subscribe((data: any) => {
            if (data.status == 'ok') {
             console.log(data,"groupmenu")
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
 }

async getAllCarterMenu(){

this.alert.Loader.show("Loading")
this.slideImage()
// this.getAllGroupList(this.caterdata.catt_id)
 	let data1:any = {
 		Caterer_id:'',
 		page_no: 1,
 		page_count:10
 	}

let isMenuthere = false
 this.groupedMenus = []
let menuWithGroups:any = []
         data1.Caterer_id = this.caterdata.catt_id

 	  await this.user.getAllMenufromCarter(data1).subscribe((data: any) => {
            if (data.status == 'ok') {
               console.log(data,"from api")
          
       if(data.data.total){
         let temp:any =  data.data.total/10
         this.totalPages = parseInt(temp) + 1;
         this.hasMoreData = true
      }


               for(let i = 0;i<data.data.menu.length;i++){
                 data.data.menu[i].number = this.minVal
                 this.MenuList.push(data.data.menu[i])

                 if(data.data.menu[i].groupss != "" && data.data.menu[i].groupss != null){
                    if(data.data.menu[i].groupss.group != "" && data.data.menu[i].groupss.group != null){
                        if(this.menuGroups.length == 0){
                        this.menuGroups.push(data.data.menu[i].groupss.group.menugroup_name)
                        this.groupedMenus.push(data.data.menu[i])
                      }
                      else{
                        for (var j = 0; j < this.menuGroups.length; ++j) {
                          if(this.menuGroups[j] == data.data.menu[i].groupss.group.menugroup_name){
                              isMenuthere = true
                          }
                        }

                        if(isMenuthere == true){
                          console.log("do nothing")
                        }
                        else{
                          this.menuGroups.push(data.data.menu[i].groupss.group.menugroup_name)
                          this.groupedMenus.push(data.data.menu[i])
                        }

                      }
                    }
                  }

                 
                  if(data.data.menu[i].groupss){
                    if(data.data.menu[i].groupss.group){
                      if(data.data.menu[i].groupss.group.menugroup_name){
                        menuWithGroups.push(data.data.menu[i])
                      }
                    }
                  }
                     
                  if(data.data.menu[i].images.length > 0){
                    data.data.menu[i].incart = false
                    this.recommendedMenu.push(data.data.menu[i])
                  }
                }


                for (var k = 0; k < this.menuGroups.length; ++k) {
                  let temp = []
                  for (var l = 0; l < menuWithGroups.length; ++l) {
                     if(this.menuGroups[k] == menuWithGroups[l].groupss.group.menugroup_name){
                       temp.push(menuWithGroups[l])
                     }
                  }
                  this.AllMenu.push(temp)
                }

                if(this.recommendedMenu.length > 0){
                  this.isRecommended = true
                  this.showImage = true
                  this.AllMenu = [this.recommendedMenu].concat(this.AllMenu)

                  let newFirstElement = "Recommended"

                  this.menuGroups = [newFirstElement].concat(this.menuGroups)
                }




                this.tabs = this.menuGroups
                console.log(this.recommendedMenu,"recommended")
                console.log(this.menuGroups,"groups")
                console.log(this.groupedMenus,"groupedMenus")
                console.log(this.AllMenu,"all menus")
                this.checkCart()
                 this.alert.Loader.hide()
               
              }
            else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })    
 }

    getMenuImage(img:any){
     let data = this.ImagePath + img;
     return data
   }


checkCart(){
 
   let token = localStorage.getItem("token")
     this.user.ViewCartItems(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               console.log(data,"cart items")
               this.cartItem = data.data;
                if(this.cartItem.length > 0){



              for(var j = 0 ; j < this.cartItem.length; j++){

                for(var i = 0 ;i < this.AllMenu.length; i++){
                  for(var k = 0;k < this.AllMenu[i].length; k++){

                 
                   if(this.AllMenu[i][k].menu_id == this.cartItem[j].menu_id){
                     this.AllMenu[i][k].incart = true
                     this.AllMenu[i][k].cart_id = this.cartItem[j].cart_id
                     this.AllMenu[i][k].number = this.cartItem[j].quantity
                   }
                   else{
                   	if(this.AllMenu[i][k].incart == false){
                     this.AllMenu[i][k].incart = false
                     this.AllMenu[i][k].cart_id = ''
                     this.AllMenu[i][k].number = this.minVal
                 }
                }
               }


                }
               }
              }
              else{
                 for(var i = 0 ;i < this.AllMenu.length; i++){
                  for(var k = 0;k < this.AllMenu[i].length; k++){
                     this.AllMenu[i][k].incart = false
                     this.AllMenu[i][k].cart_id = ''
                     this.AllMenu[i][k].number = this.minVal
                  }
                }

              } 
                  console.log(this.AllMenu,"check here")

              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
 
}

isComboCheck(value){
  if(value.is_combo == true){
  if(value.combos.length > 0){
   if(value.cart_id){
     this.addtoCart(value)
   }
   else{
    this.navCtrl.push("MenuDetailPage",{
            data: value,
            caterData: this.caterdata
         })
   }
  }
}
else{
  this.addtoCart(value)
}
}

addtoCart(value:any){

let token = localStorage.getItem("token")
let data:any = {
catt_id: value.catt_id,
menu_id:value.menu_id,
price:value.per_person_cost,
menu_name:value.menu_name,
quantity:value.number,
user_id:token
}

console.log(value,"for add to cart")
           this.user.addToCart(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Added to Cart")
               value.incart = true
               this.checkCart()
               this.getCount()
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
}

getCount(){
 this._zone.run(() =>   {

  let token = localStorage.getItem("token")
         this.user.Notification(token).subscribe((data: any) => {
            if (data.status == 'ok') {
               // this.alert.Toast.show("Added to Cart")
               console.log(data,"notification data")
               this.Cart_Count = data.data.cart_count
               let count:boolean = this.Cart_Count
               this.events.publish('cart:refresh', count);
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    })
 
}

buy(){
  this.navCtrl.push("MyCartPage",{
    min:this.minVal,
    max:this.maxVal
  })
}

numberWithCommas(n) {
   var parts=n.toString().split(".");
   return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

openCaterDetail(){
  this.navCtrl.push("CatererInfoPage",{
    catt:this.caterdata
  })
}



    minus(index,num,id){

  if(this.AllMenu[this.indexValue][index].number == this.minVal){
  console.log("reached min value")
  }else{
    this.AllMenu[this.indexValue][index].number = this.AllMenu[this.indexValue][index].number - 1;
    let data:any = {
      quantity: '',
      item_id: ''
    }
    data.quantity = this.AllMenu[this.indexValue][index].number
    data.item_id = id
    console.log(data,"before api hit")
 this._zone.run(() =>   {    
    this.user.UpdateCartItem(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Cart Updated")
              
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  })
  }
  }

 plus(index,num,id){
   if(this.AllMenu[this.indexValue][index].number == this.maxVal){
     console.log("reached max value")
   }
   else{
    this.AllMenu[this.indexValue][index].number = this.AllMenu[this.indexValue][index].number + 1;
      let data:any = {
      quantity: '',
      item_id: ''
    }
    data.quantity = this.AllMenu[this.indexValue][index].number
    data.item_id = id
    console.log(data,"before api hit")
  this._zone.run(() =>   {   
    this.user.UpdateCartItem(data).subscribe((data: any) => {
            if (data.status == 'ok') {
               this.alert.Toast.show("Cart Updated")
              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
  })
   }
 }



checkInCart(menu,operator,index,num,id){
  
    if(menu.is_combo == true){
      if(menu.combos.length > 0){
         if(!menu.cart_id){
           this.navCtrl.push("MenuDetailPage",{
            data: menu,
            caterData: this.caterdata
         })
        }
        else{
          let token = localStorage.getItem("token")
     debugger
     this.user.ViewCartItems(token).subscribe((data: any) => {
       let menuInCart = false
            if (data.status == 'ok') {
               console.log(data,"cart items")
               this.cartItem = data.data;
                if(this.cartItem.length > 0){
                 for(var j = 0 ; j < this.cartItem.length; j++){
                   if(menu.menu_id == this.cartItem[j].menu_id){
                     menuInCart = true
                   }
                 }

                 if(menuInCart == true){
                  if(operator == "plus"){
                    this.plus(index,num,id)
                  }
                  else{
                    this.minus(index,num,id)
                  }
                 }
                 else{
                     if(operator == "plus"){
                       this.plusCount(index,num)
                     }
                     else{
                       this.minusCount(index,num)
                     }
                 }
              }
              else{
                    if(operator == "plus"){
                       this.plusCount(index,num)
                     }
                     else{
                       this.minusCount(index,num)
                     }
              } 


              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        }   
      }
    }

    else {

     let token = localStorage.getItem("token")
     debugger
     this.user.ViewCartItems(token).subscribe((data: any) => {
       let menuInCart = false
            if (data.status == 'ok') {
               console.log(data,"cart items")
               this.cartItem = data.data;
                if(this.cartItem.length > 0){
                 for(var j = 0 ; j < this.cartItem.length; j++){
                   if(menu.menu_id == this.cartItem[j].menu_id){
                     menuInCart = true
                   }
                 }

                 if(menuInCart == true){
                  if(operator == "plus"){
                    this.plus(index,num,id)
                  }
                  else{
                    this.minus(index,num,id)
                  }
                 }
                 else{
                     if(operator == "plus"){
                       this.plusCount(index,num)
                     }
                     else{
                       this.minusCount(index,num)
                     }
                 }
              }
              else{
                    if(operator == "plus"){
                       this.plusCount(index,num)
                     }
                     else{
                       this.minusCount(index,num)
                     }
              } 


              }
            else {
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
}


  minusCount(index,num){
console.log(num)

  if(this.AllMenu[this.indexValue][index].number == this.minVal){
  console.log("reached 0")
  
  }else{
    this.AllMenu[this.indexValue][index].number = this.AllMenu[this.indexValue][index].number - 1;
  }
  }

 plusCount(index,num){
   if(this.AllMenu[this.indexValue][index].number == this.maxVal){
     console.log("reached 10")
   }
   else{
    this.AllMenu[this.indexValue][index].number = this.AllMenu[this.indexValue][index].number + 1;
   }
 }


}
