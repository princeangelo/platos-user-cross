import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CatererDetailPage } from './caterer-detail';
import { LongPressModule } from 'ionic-long-press';
import { ShrinkingSegmentHeaderComponent } from '../../components/shrinking-segment-header/shrinking-segment-header';


@NgModule({
  declarations: [
    CatererDetailPage,
    ShrinkingSegmentHeaderComponent,
  ],
  imports: [
    LongPressModule,
    IonicPageModule.forChild(CatererDetailPage),
  ],
})
export class CatererDetailPageModule {}
