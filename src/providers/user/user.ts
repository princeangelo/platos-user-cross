import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share'
import { Events } from 'ionic-angular';

@Injectable()

export class UserProvider {
_user: any;
  constructor(public events:Events,public http: HttpClient,public api: Api) {

    console.log('Hello UserProvider Provider');
  }

  toSignUp(data:any) {

        let seq = this.api.post('UserSignup/',{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    } 
    toSignUp_IMG(id,data:any) {

        let seq = this.api.put('UserSignup/',+id,{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    } 
    UpdateProfile(id,data:any) {

        let seq = this.api.put('UserSignup/'+id,{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }


     ChangePassword(id,data:any) {

        let seq = this.api.post('User_resetpassword/'+id,{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }





    getAllGroupMenu(data:any){
         let seq = this.api.post('getuser_grouplist/',data).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
    event_submit(data:any) {

        let seq = this.api.post('Event_Catering/',{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
    Filter_submit(data:any) {

        let seq = this.api.post('GetFilter_Caterers/',{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
    contract_submit(data:any) {

        let seq = this.api.post('Contract_Catering/',{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
    toLogin(data:any) {

        let seq = this.api.post('UserLogin/',{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

    forgot(mail: any) {
        let seq = this.api.post('Forgot_password/', {data:mail}).share();
        seq.subscribe((res: any) => {
            // If the API returned a successful response, mark the user as logged in
            if (res.status == 'success') {
               console.log("forgot done")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }

    getOrder(data:any) {
        let seq = this.api.post('GetAll_Caterers',{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
     

    // getOrder(id:any) {

    //     let seq = this.api.get('GetAll_Caterers?'+'page_no='+id+'&page_count=10').share();
    //     seq.subscribe((res: any) => {
    //         if (res.status == 'success') {} else {}
    //     }, err => {
    //         console.error('ERROR', err);
    //     });
    //     return seq;
    // }

     getMealTypes() {

        let seq = this.api.get('getall_mealtypes/').share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
    getCusineTypes() {

        let seq = this.api.get('getall_cuisine/').share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
    getPackTypes() {

        let seq = this.api.get('getall_package/').share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
    getPinCod() {

        let seq = this.api.get('GellAll_Pincodes/').share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

     getAllMenufromCarter(data:any) {
        let seq = this.api.post('All_Usermenulist/',{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
     
        addToCart(data:any) {

        let seq = this.api.post('Addto_Cart/',{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

    Notification(token){
         let seq = this.api.post('GetMy_Notification/',{"token":token}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

 ViewCartItems(token){
         let seq = this.api.post('ViewMyCart_items/',{"token":token}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

    DeleteCartItem(id){
         let seq = this.api.delete('DeleteCart_item/'+id).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    } 

    Filterdata(data){
        let seq = this.api.post('GetFilter_Caterers/',{"data":data}).share();
       seq.subscribe((res: any) => {
           if (res.status == 'success') {} else {}
       }, err => {
           console.error('ERROR', err);
       });
       return seq;
   } 


     UpdateCartItem(data){
         let seq = this.api.post('UpdateCart_item/',{"data":data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    } 

     UserDetails(token){
         let seq = this.api.post('UserDetails/',{"token":token}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

  PlaceOrder(orders){
         let seq = this.api.post('OrderPlace/',{"data":orders}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

    AddNewAddress(address){
         let seq = this.api.post('AddShipping_adress/',{"data":address}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

    AddBillAddress(address){
         let seq = this.api.post('AddBilling_adress/',{"data":address}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }


        DeleteShippAddress(id){
         let seq = this.api.delete('Shipping_addDelete/'+id).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

      DeletebilingAddress(id){
         let seq = this.api.delete('Billing_addDelete/'+id).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }


    // getCaterer(id: number) {
    //     let seq = this.api.post('UserDetails/',id).share();
    //     seq.subscribe((res: any) => {
           
    //         if (res.status == 'success') {
                
    //         }
    //     }, err => {
    //         console.error('ERROR', err);
    //     });
    //     return seq;
    // }

    deleteOtherCertificates(data:any) {
        let seq = this.api.post('Delete_gstcertificate/',{"data" : data}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }  




        checkLogin() {
        if ((localStorage.getItem("token") !== null && localStorage.getItem("token") !== "")) {
            return true;
        }
        return false;
    }


        logout() {
        this._user = null;
        localStorage.clear();
    }


    deleteCertificate(data: any) {
        let seq = this.api.post('Delete_gstcertificate/',{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
    deleteCertificate1(data: any) {
        let seq = this.api.post('Delete_pancertificate/',{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }
        setDownloadPath(value){

        localStorage.setItem("DownloadPath",value);

    }

    getDownloadPath(){

       return localStorage.getItem("DownloadPath") || "";
    }


}