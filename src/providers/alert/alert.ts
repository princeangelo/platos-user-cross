import {
    Injectable
} from '@angular/core';
import {
    AlertController,
    ToastController,
    LoadingController
} from 'ionic-angular';


@Injectable()


export class AlertProvider {
    constructor(private _alert: AlertController, private _toastCtrl: ToastController, private loadingCtrl: LoadingController) {

    }
    
    TrnsData: any = [];
   /**
    * Variable to store toast message.
    */
    public _toastMsg;
   /**
    * Variable to store loading .
    */
    public loading;
   /**
    * It creates the loading popup.
    */
    public Loader = {
        show: (data) => {
            this.loading = this.loadingCtrl.create({
                content: data ? data : "Loading ...",
            });
            this.loading.present();
        },
        hide: () => {
            this.loading.dismiss();
        }
    }
/**
 * Global declaration for Alert message.
 */
    public Alert = {
        confirm: (msg ? , title ? , okay ? , cancel ? ) => {
            return new Promise((resolve, reject) => {
                let alert = this._alert.create({
                    title: title || 'Confirm',
                    message: msg ,
                    buttons: [{
                        text: cancel || ['CANCEL'],
                        role: 'cancel',
                        handler: () => {
                            reject(false);
                        }
                    }, {
                        text: okay || ['OKAY'],
                        handler: () => {
                            resolve(true);
                        }
                    }]
                });
                alert.present();
            });
            // this.shared.Alert.confirm().then((res) => {
            //          console.log('confirmed');
            //      }, err => {
            //          console.log('user cancelled');
            //      })
        },
        alert: (msg, title ? ) => {
            let alert = this._alert.create({
                title: title || '',
                subTitle: msg,
                buttons: ['DISMISS']
            });
            alert.present();
        }
    }
/**
 * Global declaration for Toast message.
 */
    public Toast = {
        show: (text: string, duration ? , position ? , closeButton ? , btnText ? ) => {
            this._toastMsg = this._toastCtrl.create({
                message: text,
                duration: duration || closeButton ? null : 3000,
                position: position || 'bottom',
                showCloseButton: closeButton || false,
                closeButtonText: btnText || 'OK'
            });
            this._toastMsg.present();
        },
        hide() {
            this._toastMsg.dismiss();
        }
    }

    presentConfirmExit():any {

 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Exit',
    message: 'Are You Sure To Exit the App?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          resolve(true);
          console.log('Buy clicked');
        }
      }
    ]
  });
  alert.present();
              });
          }



    presentConfirm():any {

 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Confirm',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Confirm',
        handler: () => {
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }


   presentCancel():any {

 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Cancel',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }


 presentOuttoDelivery():any {

 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Out To Delivery',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }


 presentDelivered():any {

 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Delivered',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }



      logout():any {

 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Logout',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }

presentPromptFeedBack() {
   return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Cancellation',
    inputs: [
      {
        name: 'Reason',
        placeholder: 'Reason for Cancelling..'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
           reject(false);
          console.log('Cancel clicked');
        }
      },
       {
        text: 'Submit',
        handler: (data) => {
          if (data) {
            console.log(data,"feedback")
            localStorage.setItem("feedback",data.Reason)
            resolve(true);
          } 
        }
      }
    ]
  });
  alert.present();
   });
}
    
}
