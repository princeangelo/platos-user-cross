import {
    HttpClient,
    HttpParams
} from '@angular/common/http';
import {
    Injectable
} from '@angular/core';

import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { Observable } from 'rxjs/Observable';


// import {
//     UniqueDeviceID
// } from '@ionic-native/unique-device-id';
/**
 * Generated class for the Api.
 */             
@Injectable()
export class Api {
    url: string ='http://13.234.1.45/api/';
    TESTIMONIAL_IMAGE='http://13.234.1.45/images/testimonial/';
    Food_certificate='http://13.234.1.45/images/user/otherimage/';
    GST_certificate='http://13.234.1.45/images/user/gstimage/';
    PAN_certificate='http://13.234.1.45/images/user/panimage/';
    profile_pic = 'http://13.234.1.45/images/caterer/profile_pic/';
    Menu_image='http://13.234.1.45/images/menu_images/';
    filepath='http://13.234.1.45/images/caterer/fileimage/'
  //  Food_certificate='http://13.234.1.45/images/caterer/foodimage/';
  //  GST_certificate='http://13.234.1.45/images/caterer/gstimage/';

    //url: string = 'http://platos.in/api/';
    // TESTIMONIAL_IMAGE='http://platos.in/images/testimonial/';
    // Food_certificate='http://platos.in/images/caterer/foodimage/';
    // GST_certificate='http://platos.in/images/caterer/gstimage/';
    // profile_pic = 'http://platos.in/images/caterer/profile_pic/';
    // Menu_image='http://platos.in/images/menu_images/';
    // filepath='http://platos.in/images/caterer/fileimage/'

    constructor(public http: HttpClient) {
      /**
       * Getting Unique device id.
       */
    //     this.uniqueDeviceID.get().then((uuid: any) => {
    //         console.log(uuid)
    //         this.unique = uuid;
    //     }).catch((error: any) => {
    //         console.log(error)
    //     });
    }
     /**
      * Method used to get data from server.
      */
    get(endpoint: string, params ? : any, reqOpts ? : any) {
        if (!reqOpts) {
            reqOpts = {
                params: new HttpParams()
            };
        }
        // Support easy query params for GET requests
        if (params) {
            reqOpts.params = new HttpParams();
            for (let k in params) {
                reqOpts.params = reqOpts.params.set(k, params[k]);
            }
        }
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.get(this.url + endpoint, reqOpts);
    }
     /**
      * Method used to send data to server.
      */
    post(endpoint: string, body: any, reqOpts ? : any) {
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.post(this.url + endpoint, body, reqOpts);
    }
     /**
      * Set the headers.
      */
    getRetOpts(reqOpts) {
        if (!reqOpts) {
            reqOpts = {}
        }
        // reqOpts.headers = {
        //     'Content-Type': 'application/json',
        //     'Accept': 'application/json',
        //     "Authorization": `Bearer ${this.get_token()}`,
        //     'x-client-data': this.unique,
        //     "x-client-timezone": new Date().toString(),
        //     "x-client-type": "ANDROID",
        //     "x-client-country": 'india',
        // };
        return reqOpts;
    }
     /**
      * Getting Token from Local storage.
      */
    get_token() {
        return localStorage.getItem("token");
    }
     /**
      * Getting Token from Local storage.
      */
    put(endpoint: string, body: any, reqOpts ? : any) {
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.put(this.url + endpoint, body, reqOpts);
    }
     /**
      * Delete data from server.
      */
    delete(endpoint: string, reqOpts ? : any) {
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.delete(this.url + endpoint, reqOpts);
    }
     /**
      * Bulk Update.
      */
    patch(endpoint: string, body: any, reqOpts ? : any) {
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.patch(this.url + endpoint, body, reqOpts);
    }


     // intercept(observable: Observable<Response>): Observable<Response> {
     //        return observable.catch((err, source) => {
     //      if (err.status  == 401 && !_.endsWith(err.url, 'api/auth/login')) {
     //                this._router.navigate(['/login']);
     //                return Observable.empty();
     //            } else {
     //                return Observable.throw(err);
     //      }
     //        });

     //  }


}
